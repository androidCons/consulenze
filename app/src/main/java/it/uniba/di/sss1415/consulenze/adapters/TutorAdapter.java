package it.uniba.di.sss1415.consulenze.adapters;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.activities.ValutaTutor;
import it.uniba.di.sss1415.consulenze.entities.Tutor;

/**
 * Adapter per la visualizzazione dei tutor iscritti.
 * By Emanuele Pio Barracchia
 */
public class TutorAdapter extends RecyclerView.Adapter<TutorAdapter.TutorViewHolder> {

    ArrayList<Tutor> tutor;

    public TutorAdapter(ArrayList<Tutor> tutor) {
        this.tutor = tutor;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public TutorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_tutor, parent, false);
        TutorViewHolder rvh = new TutorViewHolder(v);
        return rvh;
    }

    @Override
    public void onBindViewHolder(final TutorViewHolder holder, int position) {
        final String nome = tutor.get(position).getNome();
        final String cogn = tutor.get(position).getCogn();
        holder.medico.setText(tutor.get(position).getNome() + " " + tutor.get(position).getCogn());
        holder.score.setText(String.valueOf(tutor.get(position).getScore()));
        final String score = holder.score.getText().toString();
        final String medico = holder.medico.getText().toString();
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ValutaTutor.class);
                intent.putExtra("NomeT", nome);
                intent.putExtra("CognT", cogn);
                intent.putExtra("Score", score);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tutor.size();
    }


    public static class TutorViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView medico;
        TextView score;


        public TutorViewHolder(View v) {
            super(v);
            cv = (CardView) v.findViewById(R.id.cvTutor);

            medico = (TextView) v.findViewById(R.id.txtVTutor);
            score = (TextView) v.findViewById(R.id.txtVScore);


        }
    }
}

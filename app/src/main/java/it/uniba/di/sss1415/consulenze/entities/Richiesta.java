package it.uniba.di.sss1415.consulenze.entities;


public class Richiesta {
    private String id;
    private String data;
    private String oraInizio;
    private String oraFine;
    private String intervento;
    private String medico;

    public Richiesta(String id, String data,String oraInizio,String oraFine,String intervento,String medico){
        super();
        this.id = id;
        this.data = data;
        this.oraInizio = oraInizio;
        this.oraFine = oraFine;
        this.intervento = intervento;
        this.medico = medico;
    }

    public String getId() {return id;}

    public String getData(){
        return data;
    }

    public String getOraInizio(){
        return oraInizio;
    }

    public String getOraFine(){
        return oraFine;
    }

    public String getIntervento(){
        return intervento;
    }

    public String getMedico(){
        return medico;
    }


}

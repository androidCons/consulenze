package it.uniba.di.sss1415.consulenze.database;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import it.uniba.di.sss1415.consulenze.R;

/**
 * Classe che si occupa della ricerca delle brache mediche
 */
public class BrancheMedicheDB {
  private String nome = "nome";
  private String urlString = null;

  private static final String TIPO_ELEMENTO="brancheMediche";
  private static final String ACCESSO = "read";

  public volatile boolean parsingComplete = true;
  public BrancheMedicheDB(String url){
    this.urlString = url;
  }

  public String getName(){
    return nome;
  }

  public String generaParametri(String tipoElemento, String accesso, String jsonDaInviare){
    //parametri = 'accesso:' + accesso + ', elemento:' + this.tipoElemento + ', jsonDaScrivere:' + jsonDaInviare;
    String stringaP;
    stringaP = "accesso:" + accesso + ", elemento:" + tipoElemento + ", jsonDaScrivere:" + jsonDaInviare;
    return stringaP;
  }

  public void inviaRichiesta(){
    Thread thread = new Thread(new Runnable() {
      @Override
      public void run() {
        //NON OSO TOCCARE DA QUA
        try{

          String urlParameters = generaParametri(TIPO_ELEMENTO, ACCESSO, "");
          byte[] postData = urlParameters.toString().getBytes("UTF-8");
          int postDataLength = postData.length;
          URL url = new URL(urlString);
          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
          conn.setReadTimeout(10000);
          conn.setConnectTimeout(15000);
          conn.setRequestMethod("POST");
          conn.setDoInput(true);
          conn.setDoOutput(true);
          conn.setInstanceFollowRedirects(false);
          conn.setRequestProperty("Content-Type", "application/json");
          conn.setRequestProperty("Accept", "application/json");
          conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
          conn.setUseCaches(false);
          conn.getOutputStream().write(postData);
          conn.connect();
          InputStream stream = conn.getInputStream();
          String data = convertStreamToString(stream);
          //A QUA

          Log.i("RISULTATO " + data, new StringBuilder().toString()); //QUA PUO USCIRE IL JSON O LA STRINGA
          stream.close();
          //ASSOCIO ALLA VARIABILE IL RISULTATO DELLA RICHIESTA
          nome = data;
          parsingComplete = false;
        } catch (MalformedURLException e) {
          e.printStackTrace();
        } catch (ProtocolException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });
    thread.start();
  }
  static String convertStreamToString(java.io.InputStream is) {
    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
    return s.hasNext() ? s.next() : "";
  }
}

package it.uniba.di.sss1415.consulenze.database;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.entities.Tutor;

/**
 * Classe per la connessione al server per la gestione dei tutor.
 * By Emanuele Pio Barracchia
 */
public class TutorDB {

    private JSONArray tutorArray;
    ArrayList<Tutor> tutor = new ArrayList<Tutor>();
    private JSONObject oggettoJson;

    private String urlString = null;

    private static final String TIPO_ELEMENTO = "tutor";
    private static final String ACCESSO = "read";
    private static final String ACCESSO_CHANGE = "change";

    public volatile boolean parsingComplete = true;

    public TutorDB(String url) {
        this.urlString = url;
    }

    public TutorDB(String url, String nomeT, String cognT, String scoreT) {
        urlString = url;
        oggettoJson = new JSONObject();
        try {
            oggettoJson.put("nomeT", nomeT);
            oggettoJson.put("cognomeT", cognT);
            oggettoJson.put("scoreT", scoreT);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Tutor> getTutor() {
        return tutor;
    }

    public String generaParametri(String tipoElemento, String accesso, String jsonDaInviare) {
        //parametri = 'accesso:' + accesso + ', elemento:' + this.tipoElemento + ', jsonDaScrivere:' + jsonDaInviare;
        String stringaP;
        stringaP = "accesso:" + accesso + ", elemento:" + tipoElemento + ", jsonDaScrivere:" + jsonDaInviare;
        return stringaP;
    }

    public void modificaScore() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    String urlParameters = generaParametri(TIPO_ELEMENTO, ACCESSO_CHANGE, oggettoJson.toString());
                    Log.i("PARAMETRI ", urlParameters);
                    byte[] postData = urlParameters.toString().getBytes("UTF-8");
                    int postDataLength = postData.length;
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    conn.getOutputStream().write(postData);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    String data = convertStreamToString(stream);

                    Log.i("RISULTATO " + data, new StringBuilder().toString());
                    parsingComplete = false;
                    stream.close();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public void inviaRichiesta() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    String urlParameters = generaParametri(TIPO_ELEMENTO, ACCESSO, "");
                    byte[] postData = urlParameters.toString().getBytes("UTF-8");
                    int postDataLength = postData.length;
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    conn.getOutputStream().write(postData);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    String data = convertStreamToString(stream);
                    Log.i("RISULTATO " + data, new StringBuilder().toString());
                    elaboraJson(data);
                    stream.close();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public void elaboraJson(String result) {
        oggettoJson = null;
        try {
            oggettoJson = new JSONObject(result);
            System.out.println("JSON = " + oggettoJson.toString());

            tutorArray = oggettoJson.getJSONArray("tutor");

            for (int i = 0; i < tutorArray.length(); i++) {
                JSONObject tut = tutorArray.getJSONObject(i);
                tutor.add(new Tutor(
                        tut.getString("nomeT"),
                        tut.getString("cognomeT"),
                        Integer.parseInt(tut.getString("scoreT"))
                ));


            }
            parsingComplete = false;
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}



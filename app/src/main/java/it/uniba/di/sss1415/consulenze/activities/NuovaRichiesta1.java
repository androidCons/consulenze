package it.uniba.di.sss1415.consulenze.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.rey.material.widget.Button;
import com.rey.material.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.database.BrancheMedicheDB;
import it.uniba.di.sss1415.consulenze.database.InterventiMediciDB;

/**
 * Prima parte di una nuova richiesta
 */
public class NuovaRichiesta1 extends AppCompatActivity {

    private Spinner lista_branche_mediche;
    private Spinner lista_Intereventi_Medici;
    private List interventi_medici;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuova_richiesta1);

        // inizializzazione spinner branche mediche
        lista_branche_mediche = (Spinner) findViewById(R.id.spinnerSpecializzazione);
        final ArrayAdapter<String> med_adapter = new ArrayAdapter<String>
                (this,android.R.layout.simple_spinner_item,queryBrancheMediche());

        med_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lista_branche_mediche.setAdapter(med_adapter);

        //inizializzazione spinner interventi medici
        final TextView textViewInterventiMedici = (TextView) findViewById(R.id.textViewInterventoMedico);
        lista_Intereventi_Medici = (Spinner) findViewById(R.id.spinnerInterventoMedico);

        String specializzazione_selezionata = lista_branche_mediche.getSelectedItem().toString();
        interventi_medici = queryInterventiMedici(specializzazione_selezionata);

        //dopo aver selezionato la branca medica vengono visualizzati i tipi di intervento
        lista_branche_mediche.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Spinner spinner, View view, int i, long l) {
                String specializzazione_selezionata = lista_branche_mediche.getSelectedItem().toString();
                interventi_medici = queryInterventiMedici(specializzazione_selezionata);

                final ArrayAdapter<String> med_adapter1 = new ArrayAdapter<String>
                        (NuovaRichiesta1.this,
                                android.R.layout.simple_spinner_item, interventi_medici);
                med_adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                if (interventi_medici.size() != 0) {

                    lista_Intereventi_Medici.setVisibility(View.VISIBLE);
                    textViewInterventiMedici.setVisibility(View.VISIBLE);
                }
                else{
                    lista_Intereventi_Medici.setVisibility(View.INVISIBLE);
                    textViewInterventiMedici.setVisibility(View.INVISIBLE);
                }
                lista_Intereventi_Medici.setAdapter(med_adapter1);
            }
        });

        Button continua = (Button) findViewById(R.id.btnContinua);
        continua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NuovaRichiesta1.this, NuovaRichiesta2.class);

                intent.putExtra("brancaMedica", lista_branche_mediche.getSelectedItem().toString());
                if(interventi_medici.size() != 0)
                    intent.putExtra("interventoMedico", lista_Intereventi_Medici.getSelectedItem().toString());
                else intent.putExtra("interventoMedico", "");
                startActivityForResult(intent, 220);
            }
        });




    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nuova_richiesta1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<String> queryBrancheMediche(){

        BrancheMedicheDB obj = new BrancheMedicheDB(
                getString(R.string.serverQuery)
        );
        obj.inviaRichiesta();

        while(obj.parsingComplete);
        //stampa a video della stringa
        String lista = obj.getName();

        char c;
        String temp="";
        ArrayList<String> list = new ArrayList<String>();
        for (int i=0; i<lista.length();i++){
            c=lista.charAt(i);
            if (c==','|| i==lista.length()){
                list.add(temp);
                temp="";
            } else temp = temp + c;

        }
        return list;
    }

    private ArrayList<String> queryInterventiMedici(String specializzazione){

        InterventiMediciDB obj = new InterventiMediciDB(
                getString(R.string.serverQuery), specializzazione
        );
        obj.inviaRichiesta();

        while(obj.parsingComplete);
        //stampa a video della stringa
        String lista = obj.getName();

        char c;
        String temp="";
        ArrayList<String> list = new ArrayList<String>();
        for (int i=0; i<lista.length();i++){
            c=lista.charAt(i);
            if (c==','|| i==lista.length()){
                list.add(temp);
                temp="";
            } else temp = temp + c;

        }
        return list;
    }
}

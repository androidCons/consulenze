package it.uniba.di.sss1415.consulenze.entities;


public class UserData {

    private String mail;
    private String pass;
    private String score;
    private String provincia;
    private String numero;
    private String anno;
    private String nome;
    private String cognome;
    private String specializzazione;

    public UserData(){}

    /**
     * Costruttore che prende i dati dopo l'operazione di login
     * @param dati la stringa dei dati ricevuta dal server
     */
    public UserData(String dati){

        String[] datiRicevuti = dati.toString().split(",",13);
        mail = datiRicevuti[0];
        pass = datiRicevuti[1];
        score = datiRicevuti[2];
        provincia = datiRicevuti[3];
        numero = datiRicevuti[4];
        anno = datiRicevuti[5];
        nome = datiRicevuti[6];
        cognome = datiRicevuti[7];
        specializzazione = datiRicevuti[11];
    }

    /**
     * Costruttore che prende i dati dopo l'operazione di modifica del profilo
     * @param dati la stringa dei dati ricevuta dal server
     * @param b boolean=true
     */
    public UserData(String dati, boolean b){

        String[] datiRicevuti = dati.toString().split(",",13);
        mail = datiRicevuti[0];
        pass = datiRicevuti[1];
        score = datiRicevuti[2];
        provincia = datiRicevuti[3];
        numero = datiRicevuti[4];
        anno = datiRicevuti[5];
        nome = datiRicevuti[6];
        cognome = datiRicevuti[7];
        specializzazione = datiRicevuti[8];
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getAnno() {
        return anno;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getSpecializzazione() {
        return specializzazione;
    }

    public void setSpecializzazione(String specializzazione) {
        this.specializzazione = specializzazione;
    }


}

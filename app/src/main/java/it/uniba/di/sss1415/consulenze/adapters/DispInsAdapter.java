package it.uniba.di.sss1415.consulenze.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.activities.ModificaDisponibilita;
import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.entities.Disponibilita;
import it.uniba.di.sss1415.consulenze.other.Utility;

/**
 * Adapter per le disponibilita' inserite
 */
public class DispInsAdapter extends RecyclerView.Adapter<DispInsAdapter.DispInsViewHolder>{

    private Context mContext;
    private ArrayList<Disponibilita> dispList;

    public DispInsAdapter(ArrayList<Disponibilita> disp) {
        dispList = disp;

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public DispInsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_disp_ins, parent, false);
        DispInsViewHolder avh = new DispInsViewHolder(v);
        return avh;
    }

    @Override
    public void onBindViewHolder(DispInsViewHolder holder, final int position) {
        mContext = holder.cv.getContext();
        holder.data.setText(Utility.trasformaData(dispList.get(position).getData(), holder.cv.getContext()));
        holder.ora.setText("Dalle " + dispList.get(position).getOraInizio().substring(0,5) + "\nalle " + dispList.get(position).getOraFine().substring(0,5));
        holder.descr.setText("Expertise offerta:\n" + dispList.get(position).getIntervento());
        holder.repet.setText(dispList.get(position).getRipetizione());
        if(holder.repet.getText().length() != 0) holder.endDate.setText(dispList.get(position).getFineRipetizione());
        else holder.endDate.setText("");
        holder.imgBtnMod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ModificaDisponibilita.class);
                intent.putExtra("id", dispList.get(position).getId());
                intent.putExtra("data", dispList.get(position).getData());
                intent.putExtra("ora_inizio", dispList.get(position).getOraInizio());
                intent.putExtra("ora_fine", dispList.get(position).getOraFine());
                intent.putExtra("intervento", dispList.get(position).getIntervento());
                intent.putExtra("ripetizione", dispList.get(position).getRipetizione());
                intent.putExtra("fineRipetizione", dispList.get(position).getFineRipetizione());
                ((Activity) mContext).startActivityForResult(intent, 0);

            }
        });
    }



    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return dispList.size();
    }

    public static class DispInsViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView data;
        TextView ora;
        TextView descr;
        TextView repet;
        TextView endDate;
        ImageButton imgBtnMod;

        public DispInsViewHolder(View v) {
            super(v);
            cv = (CardView) v.findViewById(R.id.cvDispInv);
            data = (TextView) v.findViewById(R.id.textViewData);
            ora = (TextView) v.findViewById(R.id.textViewOrario);
            descr = (TextView) v.findViewById(R.id.textViewDescription);
            repet = (TextView) v.findViewById(R.id.textViewRepetition);
            endDate = (TextView) v.findViewById(R.id.textViewEndDate);
            imgBtnMod = (ImageButton) v.findViewById(R.id.imgBtnModify);

        }
    }
}

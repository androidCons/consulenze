package it.uniba.di.sss1415.consulenze.database;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.entities.Appuntamento;


/**
 * Classe che si occupa della connessione al server per la gestione degli appuntamenti.
 * By Emanuele Pio Barracchia
 */
public class AppuntamentiDB {

    public JSONArray appuntamentiArray;
    ArrayList<Appuntamento> appunt = new ArrayList<Appuntamento>();
    public JSONObject oggettoJson;

    private String data;
    private String oraInizio;
    private String oraFine;
    private String tipo;
    private String intervento;
    private String dottore;
    private String urlString = null;

    private static final String TIPO_ELEMENTO = "appuntamenti";
    private static final String ACCESSO = "read";
    private static final String ACCESSO_SCRITTURA = "write";

    public volatile boolean parsingComplete = true;

    public AppuntamentiDB(String url) {
        this.urlString = url;
    }

    public AppuntamentiDB(String url, String data, String oraInizio, String oraFine, String tipo, String interv, String dottore) {
        urlString = url;
        oggettoJson = new JSONObject();
        try {
            oggettoJson.put("dottore", dottore);
            oggettoJson.put("data", data);
            oggettoJson.put("intervento", interv);
            oggettoJson.put("oraInizio", oraInizio);

            oggettoJson.put("tipoAppuntamento", tipo);
            oggettoJson.put("oraFine", oraFine);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getData() {
        return data;
    }

    public ArrayList<Appuntamento> getAppunt() {
        return appunt;
    }

    public String getOraInizio() {
        return oraInizio;
    }

    public String getOraFine() {
        return oraFine;
    }

    public String getTipo() {
        return tipo;
    }

    public String getIntervento() {
        return intervento;
    }

    public String getDottore() {
        return dottore;
    }

    ;

    public String generaParametri(String tipoElemento, String accesso, String jsonDaInviare) {
        //parametri = 'accesso:' + accesso + ', elemento:' + this.tipoElemento + ', jsonDaScrivere:' + jsonDaInviare;
        String stringaP;
        stringaP = "accesso:" + accesso + ", elemento:" + tipoElemento + ", jsonDaScrivere:" + jsonDaInviare;
        return stringaP;
    }

    public void inviaRichiesta() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    String urlParameters = generaParametri(TIPO_ELEMENTO, ACCESSO, "");
                    byte[] postData = urlParameters.toString().getBytes("UTF-8");
                    int postDataLength = postData.length;
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    conn.getOutputStream().write(postData);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    String data = convertStreamToString(stream);

                    Log.i("RISULTATO " + data, new StringBuilder().toString());
                    elaboraJson(data);
                    stream.close();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public void inviaRichiestaNuovoAppuntamento() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    String urlParameters = generaParametri(TIPO_ELEMENTO, ACCESSO_SCRITTURA, oggettoJson.toString());
                    byte[] postData = urlParameters.toString().getBytes("UTF-8");
                    int postDataLength = postData.length;
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    conn.getOutputStream().write(postData);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    String data = convertStreamToString(stream);

                    Log.i("RISULTATO " + data, new StringBuilder().toString());
                    parsingComplete = false;
                    stream.close();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }


    public void elaboraJson(String result) {
        oggettoJson = null;
        try {

            //creo un nuovo oggetto Json dal risultato ottenuto dal server
            oggettoJson = new JSONObject(result);
            //visualizzo il JSON
            System.out.println("JSON = " + oggettoJson.toString());

            //prendo l'array degli appuntamenti dando in input la chiave di accesso al file
            appuntamentiArray = oggettoJson.getJSONArray("appuntamenti");

            //ora devo prendere i singoli appuntamenti nell'array
            for (int i = 0; i < appuntamentiArray.length(); i++) {
                JSONObject singoloAppuntamento = appuntamentiArray.getJSONObject(i);
                appunt.add(new Appuntamento(
                        singoloAppuntamento.getString("tipoAppuntamento"),
                        singoloAppuntamento.getString("data"),
                        singoloAppuntamento.getString("oraInizio"),
                        singoloAppuntamento.getString("oraFine"),

                        singoloAppuntamento.getString("intervento"),
                        singoloAppuntamento.getString("dottore")));

            }
            parsingComplete = false;
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}

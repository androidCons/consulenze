package it.uniba.di.sss1415.consulenze.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.activities.ModificaRichiesta;
import it.uniba.di.sss1415.consulenze.entities.Richiesta;
import it.uniba.di.sss1415.consulenze.other.Utility;

/**
 * Adapter per la visualizzazione delle richieste inviate.
 * By Emanuele Pio Barracchia
 */
public class RichInvAdapter extends RecyclerView.Adapter<RichInvAdapter.RichInvViewHolder> {

  ArrayList<Richiesta> rich;
  private Context mContext;


  public RichInvAdapter(ArrayList<Richiesta> rich) {
    this.rich = rich;
  }

  @Override
  public void onAttachedToRecyclerView(RecyclerView recyclerView) {
    super.onAttachedToRecyclerView(recyclerView);
  }

  @Override
  public RichInvAdapter.RichInvViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_rich_inv, parent, false);
    RichInvViewHolder rvh = new RichInvViewHolder(v);
    return rvh;
  }

  @Override
  public void onBindViewHolder(RichInvViewHolder holder, final int position) {
    mContext = holder.cv.getContext();
    holder.data.setText(Utility.trasformaData(rich.get(position).getData(), holder.cv.getContext()));
    holder.ora.setText(rich.get(position).getOraInizio() + "-" + rich.get(position).getOraFine());
    holder.interv.setText("Intervento di " + rich.get(position).getIntervento());
    holder.medico.setText(rich.get(position).getMedico());
    if(rich.get(position).getMedico().equals(" ")){
      holder.medico.setText("Nessun dottore richiesto.");
    }

    holder.modifica.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(mContext, ModificaRichiesta.class);
        intent.putExtra("interventoMedico", rich.get(position).getIntervento());
        intent.putExtra("id", rich.get(position).getId());
        mContext.startActivity(intent);
      }
    });


  }

  public long getItemId(int position){
    return 0;
  }

  @Override
  public int getItemCount() {
    return rich.size();
  }


  public static class RichInvViewHolder extends RecyclerView.ViewHolder {
    CardView cv;
    TextView data;
    TextView ora;
    TextView interv;
    TextView medico;
    ImageButton modifica;

    public RichInvViewHolder(View v) {
      super(v);
      cv = (CardView) v.findViewById(R.id.cvRichInv);
      data = (TextView) v.findViewById(R.id.txtVDataRichInv);
      ora = (TextView) v.findViewById(R.id.txtVOraRichInv);
      interv = (TextView) v.findViewById(R.id.txtVIntervRichInv);
      medico = (TextView) v.findViewById(R.id.txtVMedicoRichInv);
      modifica = (ImageButton) v.findViewById(R.id.imgBtnModifica);


    }
  }
}


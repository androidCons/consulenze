package it.uniba.di.sss1415.consulenze.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;

import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.database.RichInvDB;
import it.uniba.di.sss1415.consulenze.entities.Disponibilita;
import it.uniba.di.sss1415.consulenze.other.Utility;

/**
 * Adapter per le disponibilita' trovate
 */
public class DispTrovModAdapter extends RecyclerView.Adapter<DispTrovModAdapter.DispTrovModViewHolder> {
    private Context mContext;
    private ArrayList<Disponibilita> dispList;
    private String intervento;
    private boolean confermaRichiesta = false;

    public DispTrovModAdapter(ArrayList<Disponibilita> disp, String intervento) {
        dispList = disp;
        this.intervento = intervento;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public DispTrovModViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_disp_trovate, parent, false);
        DispTrovModViewHolder avh = new DispTrovModViewHolder(v);
        return avh;
    }


    public void onBindViewHolder(final DispTrovModViewHolder holder, final int position) {
        mContext = holder.cv.getContext();
        holder.data.setText(Utility.trasformaData(dispList.get(position).getData(), holder.cv.getContext()));
        holder.ora.setText("Dalle " + dispList.get(position).getOraInizio() + "\nalle " + dispList.get(position).getOraFine());
        holder.descr.setText("Expertise offerta:\n" + dispList.get(position).getIntervento());
        holder.repet.setText(dispList.get(position).getRipetizione());
        if(holder.repet.getText().length() != 0) holder.endDate.setText(dispList.get(position).getFineRipetizione());
        else holder.endDate.setText("");
        holder.imgBtnMod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
                String email = sharedPreferences.getString("Email", "ciao@afa.it") ;
                if (!confermaRichiesta){
                    confermaRichiesta=false;
                    holder.imgBtnMod.setBackgroundColor(mContext.getResources().getColor(R.color.button_material_light));

                    holder.imgBtnMod.setBackgroundColor(mContext.getResources().getColor(R.color.green));
                    confermaRichiesta=true;
                    Toast.makeText(mContext, mContext.getString(R.string.confirm_click), Toast.LENGTH_SHORT).show();
                }
                else{
                    confermaRichiesta=false;
                    holder.imgBtnMod.setBackgroundColor(mContext.getResources().getColor(R.color.button_material_light));
                    RichInvDB richInvDB1 = new RichInvDB(mContext.getString(R.string.serverQuery),
                            dispList.get(position).getId(), dispList.get(position).getData(), dispList.get(position).getOraInizio(),
                            dispList.get(position).getOraFine(), intervento, email);
                    richInvDB1.inviaRichiestaModifica();
                    Toast.makeText(mContext, mContext.getString(R.string.richiesta_modificata), Toast.LENGTH_SHORT).show();
                    ((Activity)mContext).finish();
                }
            }
        });



    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return dispList.size();
    }

    public static class DispTrovModViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView data;
        TextView ora;
        TextView descr;
        TextView repet;
        TextView endDate;
        Button imgBtnMod;

        public DispTrovModViewHolder(View v) {
            super(v);
            cv = (CardView) v.findViewById(R.id.cvDispInv);
            data = (TextView) v.findViewById(R.id.textViewData);
            ora = (TextView) v.findViewById(R.id.textViewOrario);
            descr = (TextView) v.findViewById(R.id.textViewDescription);
            repet = (TextView) v.findViewById(R.id.textViewRepetition);
            endDate = (TextView) v.findViewById(R.id.textViewEndDate);
            imgBtnMod = (Button) v.findViewById(R.id.btnInviaRichiesta);

        }
    }
}

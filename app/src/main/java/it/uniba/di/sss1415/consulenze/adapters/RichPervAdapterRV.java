package it.uniba.di.sss1415.consulenze.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.Button;

import java.util.ArrayList;
import java.util.List;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.database.AppuntamentiDB;
import it.uniba.di.sss1415.consulenze.database.RichiestePervenuteDB;
import it.uniba.di.sss1415.consulenze.entities.Richiesta;
import it.uniba.di.sss1415.consulenze.other.Utility;

/**
 * Adapter per la visualizzazione delle richieste pervenute.
 * By Emanuele Pio Barracchia
 */
public class RichPervAdapterRV extends RecyclerView.Adapter<RichPervAdapterRV.RichiesteViewHolder> {
    List<Richiesta> richiesta;
    Context mContext;

    public RichPervAdapterRV(ArrayList<Richiesta> richiesta) {
        this.richiesta = richiesta;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        mContext = recyclerView.getContext();
    }


    @Override
    public RichiesteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_rich_perv, parent, false);
        RichiesteViewHolder rvh = new RichiesteViewHolder(v);
        return rvh;
    }

    @Override
    public void onBindViewHolder(final RichPervAdapterRV.RichiesteViewHolder holder, int position) {
        final int pos = position;

        holder.data.setText(Utility.trasformaData(richiesta.get(position).getData(), holder.cv.getContext()));
        holder.ora.setText(richiesta.get(position).getOraInizio() + "-" + richiesta.get(position).getOraFine());
        holder.interv.setText("Intervento di " + richiesta.get(position).getIntervento());
        holder.medico.setText("Pervenuta dal Dott. " + richiesta.get(position).getMedico());

        final Button btnAccetta = holder.btnAccetta;
        final Button btnRifiuta = holder.btnRifiuta;
        holder.btnAccetta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnRifiuta.getText().equals("CONFERMA")) {
                    resettaPulsanti(btnRifiuta, "RIFIUTA");
                }
                if (btnAccetta.getText().equals("ACCETTA")) {
                    btnAccetta.setText(R.string.conferma);
                    Resources res = v.getResources();
                    int grigio = res.getColor(R.color.confirm_button);
                    int nero = res.getColor(R.color.carbon_black);
                    btnAccetta.setBackgroundColor(grigio);
                    btnAccetta.setTextColor(nero);
                } else {
                    if (isOnline()) {

                        new AccettaRichiesta(mContext).execute(holder);

                        richiesta.remove(pos);
                        notifyItemRemoved(pos);
                        notifyItemRangeChanged(pos, richiesta.size());
                        resettaPulsanti(holder.btnAccetta, "ACCETTA");

                    } else {
                        String error = mContext.getResources().getString(R.string.no_conn);
                        resettaPulsanti(holder.btnAccetta, "ACCETTA");
                        Toast toastMessage = Toast.makeText(mContext, error, Toast.LENGTH_LONG);
                        toastMessage.show();
                    }

                }

            }
        });

        holder.btnRifiuta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnAccetta.getText().equals("CONFERMA")) {
                    resettaPulsanti(btnAccetta, "ACCETTA");
                }
                if (btnRifiuta.getText().equals("RIFIUTA")) {
                    btnRifiuta.setText(R.string.conferma);
                    Resources res = v.getResources();
                    int grigio = res.getColor(R.color.confirm_button);
                    int nero = res.getColor(R.color.carbon_black);
                    btnRifiuta.setBackgroundColor(grigio);
                    btnRifiuta.setTextColor(nero);
                } else {
                    if (isOnline()) {
                        new RifiutaRichiesta(mContext).execute(holder);
                        richiesta.remove(pos);
                        notifyItemRemoved(pos);
                        notifyItemRangeChanged(pos, richiesta.size());
                        resettaPulsanti(holder.btnRifiuta, "RIFIUTA");


                    } else {
                        String error = mContext.getResources().getString(R.string.no_conn);
                        resettaPulsanti(holder.btnRifiuta, "RIFIUTA");
                        Toast toastMessage = Toast.makeText(mContext, error, Toast.LENGTH_LONG);
                        toastMessage.show();
                    }
                }


            }
        });

        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resettaPulsanti(holder.btnAccetta, "ACCETTA");
                resettaPulsanti(holder.btnRifiuta, "RIFIUTA");

            }
        });


    }

    @Override
    public int getItemCount() {
        return richiesta.size();
    }


    private void resettaPulsanti(com.rey.material.widget.Button button, String nome) {
        if (button.getText().equals("CONFERMA") && nome.equals("RIFIUTA")) {
            button.setText(R.string.rifiuta);
            Resources res = button.getResources();
            int rosso = res.getColor(R.color.red);
            button.setBackgroundColor(rosso);
            int bianco = res.getColor(R.color.white);
            button.setTextColor(bianco);
        } else if (button.getText().equals("CONFERMA") && nome.equals("ACCETTA")) {
            button.setText(R.string.accetta);
            Resources res = button.getResources();
            int verde = res.getColor(R.color.green);
            button.setBackgroundColor(verde);
            int nero = res.getColor(R.color.carbon_black);
            button.setTextColor(nero);
        }
    }

    public static class RichiesteViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView data;
        TextView ora;
        TextView interv;
        TextView medico;
        Button btnRifiuta;
        Button btnAccetta;

        public RichiesteViewHolder(View v) {
            super(v);
            cv = (CardView) v.findViewById(R.id.cvRichPerv);
            data = (TextView) v.findViewById(R.id.txtVDataRichPerv);
            ora = (TextView) v.findViewById(R.id.txtVOrarioRichPerv);
            interv = (TextView) v.findViewById(R.id.txtVInterventoRichPerv);
            medico = (TextView) v.findViewById(R.id.txtVMedicoRichPerv);
            btnRifiuta = (Button) v.findViewById(R.id.btnRifiuta);
            btnAccetta = (Button) v.findViewById(R.id.btnAccetta);


        }
    }

    /**
     * Controlla se la connessione ad internet e' attiva
     *
     * @return true se e' connesso, false o null altrimenti
     */
    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    /**
     * Classe asincrona per accettare una richiesta pervenuta
     */
    class AccettaRichiesta extends AsyncTask<RichiesteViewHolder, Void, Void> {

        ProgressDialog progressDialog;
        RichiesteViewHolder holder;
        Context mContext;

        public AccettaRichiesta(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        protected Void doInBackground(RichiesteViewHolder... params) {
            RichiestePervenuteDB rich = new RichiestePervenuteDB(
                    mContext.getResources().getString(R.string.serverQuery),
                    params[0].data.getText().toString(),
                    params[0].ora.getText().toString().substring(0, 5),
                    params[0].ora.getText().toString().substring(6),
                    params[0].interv.getText().toString(),
                    params[0].medico.getText().toString()
            );
            rich.inviaEliminaRichiesta();
            while (rich.parsingComplete) ;

            Resources res = mContext.getResources();
            AppuntamentiDB obj = new AppuntamentiDB(
                    res.getString(R.string.serverQuery),
                    params[0].data.getText().toString(),
                    params[0].ora.getText().toString().substring(0, 5),
                    params[0].ora.getText().toString().substring(6),
                    "Tutoraggio",
                    params[0].interv.getText().toString(),
                    params[0].medico.getText().toString()
            );


            obj.inviaRichiestaNuovoAppuntamento();
            while (obj.parsingComplete) ;

            return null;

        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(mContext.getString(R.string.caricamento));
            progressDialog.show();
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            Toast toast = Toast.makeText(mContext, "Richiesta accettata", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * Classe asincrona per rifiutare una richiesta pervenuta
     */
    class RifiutaRichiesta extends AsyncTask<RichiesteViewHolder, Void, Void> {

        ProgressDialog progressDialog;
        RichiesteViewHolder holder;
        Context mContext;

        public RifiutaRichiesta(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        protected Void doInBackground(RichiesteViewHolder... params) {
            RichiestePervenuteDB rich = new RichiestePervenuteDB(
                    mContext.getResources().getString(R.string.serverQuery),
                    params[0].data.getText().toString(),
                    params[0].ora.getText().toString().substring(0, 5),
                    params[0].ora.getText().toString().substring(6),
                    params[0].interv.getText().toString(),
                    params[0].medico.getText().toString()
            );
            rich.inviaEliminaRichiesta();
            while (rich.parsingComplete) ;


            return null;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(mContext.getString(R.string.caricamento));
            progressDialog.show();
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            Toast toast = Toast.makeText(mContext, "Richiesta rifiutata", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}



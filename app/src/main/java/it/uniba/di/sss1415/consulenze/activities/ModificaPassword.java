package it.uniba.di.sss1415.consulenze.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.blunderer.materialdesignlibrary.activities.Activity;
import com.blunderer.materialdesignlibrary.handlers.ActionBarDefaultHandler;
import com.blunderer.materialdesignlibrary.handlers.ActionBarHandler;
import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.database.ModificaPasswordDB;

/**
 * Activity che permette di modificare la password del proprio profilo personale.
 * By Fabrizio Centrone
 */
public class ModificaPassword extends AppCompatActivity {

    private EditText ETpassvecchia, ETpass1, ETpass2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifica_password);
        //prendi valori dai campi text
        ETpassvecchia = (EditText) findViewById(R.id.editTextPassVecchia);
        ETpass1 = (EditText) findViewById(R.id.editTextPassNuova1);
        ETpass2 = (EditText) findViewById(R.id.editTextPassNuova2);
        //listener dei bottoni
        Button btnupdate = (Button) findViewById(R.id.buttonConferma);
        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cambia_password();
            }
        });
        //ripristino stato precedente
        if (savedInstanceState!=null){
            ETpassvecchia.setText(savedInstanceState.getString(getString(R.string.pass_vecchia),""));
            ETpass1.setText(savedInstanceState.getString(getString(R.string.pass_nuova),""));
            ETpass2.setText(savedInstanceState.getString(getString(R.string.conf_password),""));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outstate){
        super.onSaveInstanceState(outstate);
        outstate.putString(getString(R.string.pass_vecchia), ETpassvecchia.getText().toString());
        outstate.putString(getString(R.string.pass_nuova),ETpass1.getText().toString());
        outstate.putString(getString(R.string.conf_password), ETpass2.getText().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_modifica_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * Metodo richiamato una volta premuto il tasto per cambiare la password.
     */
    public void cambia_password(){
        Context context=getApplicationContext();
        String error="";
        Toast toastMessage;

        ETpassvecchia.clearError();
        ETpass1.clearError();
        ETpass2.clearError();

        String pass_attuale, nuovapass1, nuovapass2;
        String email = "";
        pass_attuale = ETpassvecchia.getText().toString();
        nuovapass1= ETpass1.getText().toString();
        nuovapass2= ETpass2.getText().toString();

        try{
            // confronta pass attuale
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            email = sharedPref.getString(getString(R.string.email), "");
            String temp = sharedPref.getString(getString(R.string.password), "");
            if (!(temp.compareTo(pass_attuale)==0)){
                //errore la pass attuale non corrisponde
                error = getApplicationContext().getResources().getString(R.string.errorPassword2);
                ETpassvecchia.setError(error);
                toastMessage = Toast.makeText(context, error, Toast.LENGTH_LONG);
                toastMessage.show();
            } else {
                if (!nuovapass1.contains(",")){
                if (nuovapass1.compareTo(nuovapass2)==0){
                    if(isOnline()) {
                        //cambia la password
                        ModificaPasswordDB obj = new ModificaPasswordDB(getString(R.string.serverQuery),email, nuovapass1);
                        obj.inviaRichiesta();
                        while(obj.parsingComplete);
                        if (obj.getRisposta()==null || obj.getRisposta().compareTo("")==0){
                            //errore connessione db
                            error = getApplicationContext().getResources().getString(R.string.errorUpdate);
                            toastMessage = Toast.makeText(context, error, Toast.LENGTH_LONG);
                            toastMessage.show();
                        } else {
                            //password cambiata con successo
                            error = getApplicationContext().getResources().getString(R.string.passUpdate);
                            toastMessage = Toast.makeText(context, error, Toast.LENGTH_LONG);
                            toastMessage.show();
                            // Torna al menu principale
                            Intent intent = new Intent (this, MenuPrincipale.class);
                            MenuPrincipale.lastSelect=0;
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        // errore connessione internet
                        Toast.makeText(getApplicationContext(), getString(R.string.no_conn), Toast.LENGTH_LONG).show();
                    }
                } else{
                    //le due password nuove non corrispondono
                    ETpass1.setError("");
                    ETpass2.setError("");
                    error = getApplicationContext().getResources().getString(R.string.errorPassword3);
                    toastMessage = Toast.makeText(context, error, Toast.LENGTH_LONG);
                    toastMessage.show();
                }
                } else{
                    ETpass1.setError("");
                    ETpass2.setError("");
                    error = getApplicationContext().getResources().getString(R.string.invalid_value);
                    toastMessage = Toast.makeText(context, error, Toast.LENGTH_LONG);
                    toastMessage.show();
                }
            }
        } catch (Exception e){
            //errore connessione db
            Toast errordb = Toast.makeText(getApplicationContext(), getString(R.string.errorUpdate), Toast.LENGTH_LONG);
            errordb.show();
        }

    }

    /**
     * Controlla se la connessione ad internet e' attiva
     * @return true se connesso, false o null altrimenti
     */
    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

}

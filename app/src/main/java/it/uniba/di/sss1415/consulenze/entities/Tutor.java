package it.uniba.di.sss1415.consulenze.entities;


public class Tutor {
  private String nome;
  private String cogn;
  private int score;

  public Tutor(String nomeT, String cognomeT, int scoreT){
    super();
    nome = nomeT;
    cogn = cognomeT;
    score = scoreT;
  }

  public String getNome(){
    return nome;
  }

  public String getCogn(){
    return cogn;
  }

  public int getScore(){
    return score;
  }
}

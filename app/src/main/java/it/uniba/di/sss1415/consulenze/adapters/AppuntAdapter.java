package it.uniba.di.sss1415.consulenze.adapters;

import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.entities.Appuntamento;
import it.uniba.di.sss1415.consulenze.other.Utility;

/**
 * Adapter per la visualizzazione degli appuntamenti.
 * By Emanuele Pio Barracchia
 */
public class AppuntAdapter extends RecyclerView.Adapter<AppuntAdapter.AppuntViewHolder> {
    ArrayList<Appuntamento> appunt;
    Resources res;

    public AppuntAdapter(ArrayList<Appuntamento> appunt) {
        this.appunt = appunt;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        res = recyclerView.getResources();
    }

    @Override
    public AppuntAdapter.AppuntViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_appunt, parent, false);
        AppuntViewHolder avh = new AppuntViewHolder(v);
        return avh;
    }

    @Override
    public void onBindViewHolder(AppuntAdapter.AppuntViewHolder holder, int position) {
        holder.data.setText(Utility.trasformaData(appunt.get(position).getData(), holder.cv.getContext()));
        holder.ora.setText(appunt.get(position).getOraInizio() + "-" + appunt.get(position).getOraFine());
        String tipo = appunt.get(position).getTipo();
        if (tipo.equals("Ricevimento tutoraggio")) {
            holder.tipo.setImageDrawable(res.getDrawable(R.drawable.ic_walk));
            holder.interv.setText("Operazione di " + appunt.get(position).getIntervento() +
                    " assistita dal Dott. " + appunt.get(position).getMedico());
        } else {
            holder.tipo.setImageDrawable(res.getDrawable(R.drawable.briefcase));
            holder.interv.setText("Tutoraggio di " + appunt.get(position).getIntervento() +
                    " per il Dott. " + appunt.get(position).getMedico());
        }
    }

    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return appunt.size();
    }


    public static class AppuntViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView data;
        TextView ora;
        TextView interv;
        ImageView tipo;

        public AppuntViewHolder(View v) {
            super(v);
            cv = (CardView) v.findViewById(R.id.cvAppunt);
            data = (TextView) v.findViewById(R.id.txtVDataAppunt);
            ora = (TextView) v.findViewById(R.id.txtVOraAppunt);
            interv = (TextView) v.findViewById(R.id.txtVIntervAppunt);
            tipo = (ImageView) v.findViewById(R.id.imgTipoAppunt);


        }
    }
}

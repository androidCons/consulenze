package it.uniba.di.sss1415.consulenze.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;
import com.rey.material.widget.Spinner;

import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.database.BrancheMedicheDB;
import it.uniba.di.sss1415.consulenze.database.ModificaProfiloDB;
import it.uniba.di.sss1415.consulenze.database.RegistrazioneDB;

/**
 * Activity per l'operazione di registrazione al sistema.
 * By Fabrizio Centrone
 */
public class Registrazione extends AppCompatActivity {

    //campi di testo, spinner, bottoni
    private EditText ETemail, ETnome, ETcognome, ETanno, ETnumero, ETpass1, ETpass2;
    private Spinner lista_province;
    private Spinner lista_branche_mediche;
    private Button btnreg;
    private String password="";
    private String spec="";
    ArrayAdapter<CharSequence> adapter;
    ArrayAdapter<String> med_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrazione);

        //inizializza campi
        ETemail= (EditText) findViewById(R.id.editTextMail);
        ETnome= (EditText) findViewById(R.id.editTextNome);
        ETcognome= (EditText) findViewById(R.id.editTextCognome);
        ETanno= (EditText) findViewById(R.id.editTextAnno);
        ETnumero= (EditText) findViewById(R.id.editTextNumero);
        ETpass1= (EditText) findViewById(R.id.editTextPass1);
        ETpass2= (EditText) findViewById(R.id.editTextPass2);
        // inizializzazione spinner province
        lista_province = (Spinner) findViewById(R.id.spinnerProvince);
        adapter = ArrayAdapter.createFromResource(this, R.array.arrayProvince,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lista_province.setAdapter(adapter);

        // inizializzazione spinner branche mediche
        lista_branche_mediche = (Spinner) findViewById(R.id.spinnerSpecializzazioni);
        ArrayList<String> lista_spec = null;
        if (isOnline()) {
            lista_spec = queryBrancheMediche();
            med_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista_spec);
            med_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            lista_branche_mediche.setAdapter(med_adapter);
        } else {
            // errore connessione internet
            Toast.makeText(getApplicationContext(), getString(R.string.no_conn), Toast.LENGTH_LONG).show();
            spec="";
        }

        //listener bottone
        btnreg = (Button) findViewById(R.id.btnRegistrazione);
        btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrazione();
            }
        });

        //ripristino stato precedente
        if (savedInstanceState!=null){
            ETemail.setText(savedInstanceState.getString(getString(R.string.email),""));
            ETnome.setText(savedInstanceState.getString(getString(R.string.nome),""));
            ETcognome.setText(savedInstanceState.getString(getString(R.string.cognome),""));
            ETanno.setText(savedInstanceState.getString(getString(R.string.anno),""));
            ETnumero.setText(savedInstanceState.getString(getString(R.string.numero),""));
            ETpass1.setText(savedInstanceState.getString(getString(R.string.password),""));
            ETpass2.setText(savedInstanceState.getString(getString(R.string.conf_password),""));
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outstate){
        super.onSaveInstanceState(outstate);
        outstate.putString(getString(R.string.email), ETemail.getText().toString());
        outstate.putString(getString(R.string.nome),ETnome.getText().toString());
        outstate.putString(getString(R.string.cognome),ETcognome.getText().toString());
        outstate.putString(getString(R.string.anno),ETanno.getText().toString());
        outstate.putString(getString(R.string.numero),ETnumero.getText().toString());
        outstate.putString(getString(R.string.password),ETpass1.getText().toString());
        outstate.putString(getString(R.string.conf_password),ETpass2.getText().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gestione_dati_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * Metodo che ottiene dal server la lista delle specializzazioni mediche da visualizzare
     * nell'apposito spinner.
     * @return la lista delle branche mediche se la connessione � avvenuta con successo,
     * lista vuota altrimenti
     */
    private ArrayList<String> queryBrancheMediche(){

        ArrayList<String> list = new ArrayList<String>();
        if(isOnline()) {
            BrancheMedicheDB obj = new BrancheMedicheDB(getString(R.string.serverQuery));
            obj.inviaRichiesta();
            while(obj.parsingComplete);
            String lista= obj.getName();

            char c;
            String temp="";
            for (int i=0; i<lista.length();i++){
                c=lista.charAt(i);
                if (c==','|| i==lista.length()){
                    list.add(temp);
                    temp="";
                } else temp = temp + c;
            }
        } else {
            // errore connessione internet
            Toast.makeText(getApplicationContext(), getString(R.string.no_conn), Toast.LENGTH_LONG).show();
        }
        return list;
    }

    /**
     * Metodo per la registrazione dell'utente al sistema.
     * In caso di esito positivo, permette l'accesso al menu principale.
     */
    private void registrazione(){

        String mail, nome, cognome, anno, numero, pass1, pass2, provincia;
        String parametriServer="";

        ETemail.clearError();
        ETnome.clearError();
        ETcognome.clearError();
        ETanno.clearError();
        ETnumero.clearError();
        ETpass1.clearError();
        ETpass2.clearError();

        // prendi dati dalle text
        mail=ETemail.getText().toString();
        nome=ETnome.getText().toString();
        cognome=ETcognome.getText().toString();
        anno=ETanno.getText().toString();
        numero=ETnumero.getText().toString();
        pass1=ETpass1.getText().toString();
        pass2=ETpass2.getText().toString();
        provincia= lista_province.getSelectedItem().toString();

        // prendi stringa specializzazione
        spec=lista_branche_mediche.getSelectedItem().toString();

        // controllo campi
        boolean check = controllo_campi(mail, nome, cognome, anno, numero, spec);
        if (check) {
            //CONTROLLO PASSWORD
            if (!pass1.contains(",")){
                if (pass1.compareTo("") != 0 && pass1.compareTo(pass2) == 0) {

                    if (isOnline()) {
                        // async task
                        new AsyncReg(this).execute();
                    } else {
                        // errore connessione internet
                        Toast.makeText(getApplicationContext(), getString(R.string.no_conn), Toast.LENGTH_LONG).show();
                    }

                } else {
                    // errore: le password non combaciano
                    ETpass1.setError("");
                    ETpass2.setError("");
                    Context context = getApplicationContext();
                    Toast toastMessage = Toast.makeText(context, getApplicationContext().getResources().getString(R.string.errorPassword), Toast.LENGTH_LONG);
                    toastMessage.show();
                }
        } else {
                ETpass1.setError("");
                ETpass2.setError("");
                Context context = getApplicationContext();
                Toast toastMessage = Toast.makeText(context, getApplicationContext().getResources().getString(R.string.invalid_value), Toast.LENGTH_LONG);
                toastMessage.show();
            }
        }
    }

    /**
     * Metodo per il salvataggio dei dati del profilo utente sulle shared preferences
     * @param mail
     * @param nome
     * @param cognome
     * @param provincia
     * @param anno
     * @param numero
     * @param specializzazione
     * @param pass
     * @param score
     */
    private void salva_dati_utente(String mail, String nome, String cognome, String provincia, String anno, String numero, String specializzazione, String pass, String score){
        try {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.clear();

            editor.putString(getString(R.string.email), mail);
            editor.putString(getString(R.string.password), pass);
            editor.putString(getString(R.string.txtProv), provincia);
            editor.putString(getString(R.string.numero), numero);
            editor.putString(getString(R.string.anno), anno);
            editor.putString(getString(R.string.nome), nome);
            editor.putString(getString(R.string.cognome), cognome);
            editor.putString(getString(R.string.spec_prim), specializzazione);
            editor.putString(getString(R.string.score), score);
            editor.apply();
        } catch (Exception e) {}
    }

    /**
     * Metodo di controllo dei valori inseriti dall'utente in fase di registrazione.
     * La mail deve contenere una chiocciola, seguita da un punto nella parte successiva della stringa.
     * Tutti gli altri campi non devono essere vuoti e non devono contenere virgole
     * @param mail
     * @param nome
     * @param cognome
     * @param anno
     * @param numero
     * @param spec
     * @return true se tutti i valori inseriti sono validi, false altrimenti.
     */
    private boolean controllo_campi(String mail, String nome, String cognome, String anno, String numero, String spec){
        Context context = getApplicationContext();
        Toast toastMessage;
        String error="";

        //la mail deve contenere una chiocciola e un punto
        boolean checkmail=false;
        char c;
        String temp;
        for (int i=0; i<mail.length();i++){
            c = mail.charAt(i);
            if (c=='@'){
                temp=mail.substring(i);
                for (int j=0; j<temp.length();j++){
                    c=temp.charAt(j);
                    if (c=='.') checkmail=true;
                }
            }
        }
        if (!checkmail || mail.contains(",")){
            //errore mail
            error = getApplicationContext().getResources().getString(R.string.errorEmail);
            ETemail.setError(getString(R.string.errorEmail));
        } else {
            if (nome.compareTo("")==0) {error = getApplicationContext().getResources().getString(R.string.insertNome); ETnome.setError(getString(R.string.insertNome));}
            else if (nome.contains(",")) {error = getApplicationContext().getResources().getString(R.string.invalid_value); ETnome.setError(getString(R.string.invalid_value));}
            else if (cognome.compareTo("")==0) {error = getApplicationContext().getResources().getString(R.string.insertCognome);ETcognome.setError(getString(R.string.insertCognome));}
            else if (cognome.contains(",")) {error = getApplicationContext().getResources().getString(R.string.invalid_value); ETcognome.setError(getString(R.string.invalid_value));}
            else if (anno.compareTo("")==0) {error = getApplicationContext().getResources().getString(R.string.insertAnno);ETanno.setError(getString(R.string.insertAnno));}
            else if (anno.contains(",")) {error = getApplicationContext().getResources().getString(R.string.invalid_value); ETanno.setError(getString(R.string.invalid_value));}
            else if (numero.compareTo("")==0) {error = getApplicationContext().getResources().getString(R.string.insertNumero);ETnumero.setError(getString(R.string.insertNumero));}
            else if (numero.contains(",")) {error = getApplicationContext().getResources().getString(R.string.invalid_value); ETnumero.setError(getString(R.string.invalid_value));}
            else if (spec==null || spec.compareTo("")==0) {error = getApplicationContext().getResources().getString(R.string.no_conn);}
            else return true;
        }
        toastMessage = Toast.makeText(context, error, Toast.LENGTH_LONG);
        toastMessage.show();
        return false;
    }

    /**
     * Controlla se la connessione ad internet e' attiva
     * @return true se e' connesso, false o null altrimenti
     */
    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    /**
     * Classe che esegue la registrazione tramite task asincrono
     */
    class AsyncReg extends AsyncTask<String, Void, Void> {

        ProgressDialog progressDialog;
        Context mContext;
        String mail=ETemail.getText().toString();
        String nome=ETnome.getText().toString();
        String cognome=ETcognome.getText().toString();
        String anno=ETanno.getText().toString();
        String numero=ETnumero.getText().toString();
        String pass1=ETpass1.getText().toString();
        String pass2=ETpass2.getText().toString();
        String provincia= lista_province.getSelectedItem().toString();

        // prendi stringa specializzazione
        String spec=lista_branche_mediche.getSelectedItem().toString();

        public AsyncReg(Context mContext){
            this.mContext = mContext;
        }

        @Override
        protected Void doInBackground(String... params) {

            //connessione al server
            RegistrazioneDB obj = new RegistrazioneDB(getString(R.string.serverQuery), mail, numero, anno, provincia, pass1);
            obj.inviaRichiesta();
            while (obj.parsingComplete) ;
            if (obj.getRisposta().equals("Utente non presente")) {
                Context context = getApplicationContext();
                String error = getApplicationContext().getResources().getString(R.string.errorLogin);
                Toast toastMessage = Toast.makeText(context, error, Toast.LENGTH_LONG);
                toastMessage.show();
            } else {

                // modifica profilo con i dati restanti
                ModificaProfiloDB mp = new ModificaProfiloDB(getString(R.string.serverQuery), mail, nome, cognome, provincia, anno, numero, spec);
                mp.inviaRichiesta();
                while (mp.parsingComplete) ;

                //controlla se l'operazione e' riuscita o meno
                if (!(mp.getRisposta()==null || mp.getRisposta().compareTo("")==0)) {
                    salva_dati_utente(mail, nome, cognome, provincia, anno, numero, spec, pass1, getString(R.string.default_score));
                    //VAI AL MENU PRINCIPALE
                    Intent intent = new Intent(mContext, MenuPrincipale.class);
                    startActivity(intent);
                    setResult(RESULT_OK);
                    finish();
                } else {
                    //errore connessione db
                    Toast errordb = Toast.makeText(getApplicationContext(), getString(R.string.errorUpdate), Toast.LENGTH_LONG);
                    errordb.show();
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            //creazione progress dialog
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(mContext.getString(R.string.caricamento));
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            //dismiss della progress dialog
            progressDialog.dismiss();
        }
    }

}

package it.uniba.di.sss1415.consulenze.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;
import com.rey.material.widget.Spinner;
import com.rey.material.widget.TextView;

import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.activities.MenuPrincipale;
import it.uniba.di.sss1415.consulenze.activities.ModificaPassword;
import it.uniba.di.sss1415.consulenze.activities.ModificaProfilo;
import it.uniba.di.sss1415.consulenze.activities.Registrazione;

/**
 * Fragment che visualizza i dati dell'utente loggato.
 * By Fabrizio Centrone
 */
public class profilo_utente extends com.blunderer.materialdesignlibrary.fragments.ScrollViewFragment {

    @Override
    public int getContentView() {return R.layout.fragment_profilo_utente;}

    @Override
    public boolean pullToRefreshEnabled() {return true;}

    @Override
    public int[] getPullToRefreshColorResources() {return new int[0];}

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                setRefreshing(false);
            }

        }, 2000);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profilo_utente, container, false);
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MenuPrincipale.lastSelect = 3;
        MenuPrincipale.count=0;

        //inizializza campi
        TextView TVemail= (TextView) getView().findViewById(R.id.textViewEmail);
        TextView TVnome= (TextView) getView().findViewById(R.id.textViewNome);
        TextView TVcognome= (TextView) getView().findViewById(R.id.textViewCognome);
        TextView TVscore= (TextView) getView().findViewById(R.id.textViewScore);
        TextView TVanno= (TextView) getView().findViewById(R.id.textViewAnno);
        TextView TVnumero= (TextView) getView().findViewById(R.id.textViewNumero);
        TextView TVprovincia= (TextView) getView().findViewById(R.id.textViewProvincia);
        TextView TVspec= (TextView) getView().findViewById(R.id.textViewSpec);

        // prendi dati utente e popola campi
        try {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getView().getContext());
            TVemail.setText(sharedPref.getString(getString(R.string.email), ""));
            TVnome.setText(sharedPref.getString(getString(R.string.nome), ""));
            TVcognome.setText(sharedPref.getString(getString(R.string.cognome), ""));
            TVscore.setText(sharedPref.getString(getString(R.string.score), ""));
            TVanno.setText(sharedPref.getString(getString(R.string.anno), ""));
            TVnumero.setText(sharedPref.getString(getString(R.string.numero), ""));
            TVprovincia.setText(sharedPref.getString(getString(R.string.txtProv), ""));
            TVspec.setText(sharedPref.getString(getString(R.string.spec_prim), ""));
        } catch (Exception e) {}

        //listener dei bottoni
        Button btnupdate = (Button) getView().findViewById(R.id.btnAggiornaProfilo);
        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getView().getContext(), ModificaProfilo.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        Button btnpass = (Button) getView().findViewById(R.id.btnModificaPassword);
        btnpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getView().getContext(), ModificaPassword.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

}

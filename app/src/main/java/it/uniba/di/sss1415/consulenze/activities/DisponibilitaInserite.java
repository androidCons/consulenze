package it.uniba.di.sss1415.consulenze.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.adapters.DisponibilitaInseriteAdapter;
import it.uniba.di.sss1415.consulenze.database.DisponibilitaDB;
import it.uniba.di.sss1415.consulenze.entities.Disponibilita;

/**
 * Classe per le disponibilita' inserite dall'utente
 */
public class DisponibilitaInserite extends AppCompatActivity {

    ListView disponibilita;
    ArrayList<Disponibilita> dispList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disponibilita_inserite);

        DisponibilitaDB disponibilitaDB = new DisponibilitaDB(getString(R.string.serverQuery));
        disponibilitaDB.inviaRichiestaLettura();
        while(disponibilitaDB.parsingComplete);
        dispList = disponibilitaDB.getdispList();
        pulisciLista();
        disponibilita = (ListView) findViewById(R.id.listViewDisponibilitaInserite);
        disponibilita.setAdapter(new DisponibilitaInseriteAdapter(this, dispList));

    }


       @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_disponibilita_inserite, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * Metodo per eliminare le disponibilita' precedenti a oggi
     * e ordinare per data
     */
      private void pulisciLista() {
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);

          /**
           * Elimino disponibilita' passate
           */
        for (int i = 0; i < dispList.size(); i++){
            if (Integer.parseInt(dispList.get(i).getData().substring(0,4)) < year){
                dispList.remove(i);
                i = i-1;
            }

            else{
                if (Integer.parseInt(dispList.get(i).getData().substring(0,4)) == year){
                    if (Integer.parseInt(dispList.get(i).getData().substring(5,7)) < month){
                        dispList.remove(i);
                        i = i-1;
                    }
                    else{
                        if (Integer.parseInt(dispList.get(i).getData().substring(5,7)) == month) {
                            if (Integer.parseInt(dispList.get(i).getData().substring(8,10)) < day){
                                dispList.remove(i);
                                i = i-1;
                            }
                        }
                    }
                }
            }
        }

          /**
           * Ordino per data
           */
        for (int i=0; i < dispList.size(); i++){

            for (int j = 1; j < dispList.size()-i; j++){
                if(Integer.parseInt(dispList.get(j-1).getData().substring(0,4)) >
                        Integer.parseInt(dispList.get(j).getData().substring(0,4))){
                    Disponibilita temp = dispList.get(j-1);
                    dispList.set(j-1, dispList.get(j));
                    dispList.set(j, temp);
                }
                else{
                    if(Integer.parseInt(dispList.get(j-1).getData().substring(0,4)) ==
                            Integer.parseInt(dispList.get(j).getData().substring(0,4))){
                        if(Integer.parseInt(dispList.get(j-1).getData().substring(5,7)) >
                                Integer.parseInt(dispList.get(j).getData().substring(5,7))){
                            Disponibilita temp = dispList.get(j-1);
                            dispList.set(j-1, dispList.get(j));
                            dispList.set(j, temp);
                        }
                        else{
                            if (Integer.parseInt(dispList.get(j-1).getData().substring(5,7)) ==
                                    Integer.parseInt(dispList.get(j).getData().substring(5,7))){
                                if(Integer.parseInt(dispList.get(j-1).getData().substring(8,10)) >
                                        Integer.parseInt(dispList.get(j).getData().substring(8,10))){
                                    Disponibilita temp = dispList.get(j-1);
                                    dispList.set(j-1, dispList.get(j));
                                    dispList.set(j, temp);
                                }
                            }
                        }
                    }
                }

            }
        }
    }


}

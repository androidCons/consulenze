package it.uniba.di.sss1415.consulenze.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonFloat;
import com.rey.material.widget.Spinner;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.activities.MenuPrincipale;
import it.uniba.di.sss1415.consulenze.activities.NuovaRichiesta1;
import it.uniba.di.sss1415.consulenze.adapters.AppuntAdapter;
import it.uniba.di.sss1415.consulenze.adapters.RichInvAdapter;
import it.uniba.di.sss1415.consulenze.adapters.RichPervAdapterRV;
import it.uniba.di.sss1415.consulenze.database.AppuntamentiDB;
import it.uniba.di.sss1415.consulenze.database.RichInvDB;
import it.uniba.di.sss1415.consulenze.database.RichiestePervenuteDB;
import it.uniba.di.sss1415.consulenze.entities.Richiesta;
import it.uniba.di.sss1415.consulenze.other.Utility;

/**
 * Fragment relativo alla bacheca. All'interno del fragment e' presente uno spinner
 * per accedere alle funzionalità della bacheca:
 * - Richieste pervenute
 * - Appuntamenti confermati
 * - Richieste inviate
 *
 * By Emanuele Pio Barracchia
 */
public class BachecaFragmentCard extends com.blunderer.materialdesignlibrary.fragments.ScrollViewFragment{
  public static Richiesta[] richiestePub;

  RichPervAdapterRV richPervAdapter;
  AppuntAdapter appuntAdapter;
  RichInvAdapter richInvAdapter;
  public static int lastSelect = 0;

  Spinner spinner;
  RecyclerView rv;
  @Override
  public int getContentView() {
    return R.layout.fragment_bacheca;
  }

  @Override
  public boolean pullToRefreshEnabled() {
    return true;
  }

  @Override
  public int[] getPullToRefreshColorResources() {
    return new int[0];
  }

  @Override
  public void onRefresh() {
    new Handler().postDelayed(new Runnable() {

      @Override
      public void run() {
        setRefreshing(false);
      }

    }, 2000);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);


  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_bacheca, container, false);


  }


  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    MenuPrincipale.lastSelect = 0;
    MenuPrincipale.count = 0;
    rv = (RecyclerView) getView().findViewById(R.id.cardListBacheca);
    LinearLayoutManager llm = new LinearLayoutManager(getActivity().getApplicationContext());
    rv.setLayoutManager(llm);

    spinner = (Spinner) getView().findViewById(R.id.spinnerBacheca);
    String[] funz_spinner = getView().getResources().getStringArray(R.array.bacheca);
    ArrayAdapter<String> bacheca = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, funz_spinner);
    spinner.setAdapter(bacheca);
    spinner.setSelection(lastSelect);



    if (isOnline()){
      riempiRecyclerView();

      //spinner
      spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(Spinner spinner, View view, int i, long l) {
          riempiRecyclerView();
        }
      });
    } else {
      String error = getActivity().getApplicationContext().getResources().getString(R.string.no_conn);
      Toast toastMessage = Toast.makeText(getActivity().getApplicationContext(), error, Toast.LENGTH_LONG);
      toastMessage.show();
    }

  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.menu_bacheca, menu);


  }

  private void riempiRecyclerView(){
    if (spinner.getSelectedItemPosition() == 0) {
      lastSelect = 0;
      //Richieste pervenute
      RichiestePervenuteDB richPerv = new RichiestePervenuteDB(getString(R.string.serverQuery));
      richPerv.inviaRichiestaLettura();
      while(richPerv.parsingComplete);
      Utility.ordinaRichiesta(richPerv.getRichPerv());
      richPervAdapter = new RichPervAdapterRV(richPerv.getRichPerv());
      rv.setAdapter(richPervAdapter);
    } else if (spinner.getSelectedItemPosition() == 1) {
      lastSelect = 1;
      //Appuntamenti
      AppuntamentiDB appunt = new AppuntamentiDB(getString(R.string.serverQuery));
      appunt.inviaRichiesta();
      while (appunt.parsingComplete) ;

      Utility.ordinaAppunt(appunt.getAppunt());
      //Creo l'adapter
      appuntAdapter = new AppuntAdapter(appunt.getAppunt());

      rv.setAdapter(appuntAdapter);
    } else {
      lastSelect = 2;
      //Richieste inviate
      RichInvDB richInvDB = new RichInvDB(getString(R.string.serverQuery));
      richInvDB.inviaRichiesta();
      while (richInvDB.parsingComplete) ;
      Utility.ordinaRichiesta(richInvDB.getRichInv());
      richInvAdapter = new RichInvAdapter(richInvDB.getRichInv());
      rv.setAdapter(richInvAdapter);
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();

    if (id == R.id.menu_nuova_richiesta) {
      Intent intent = new Intent(getView().getContext(), NuovaRichiesta1.class);
      startActivityForResult(intent, 11);
    }

    return super.onOptionsItemSelected(item);
  }

  /**
   * Controlla se la connessione ad internet e' attiva
   * @return true se e' connesso, false o null altrimenti
   */
  public boolean isOnline() {
    ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    return activeNetworkInfo != null;
  }
}

package it.uniba.di.sss1415.consulenze.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.activities.MenuPrincipale;
import it.uniba.di.sss1415.consulenze.activities.NuovaDisponibilita;
import it.uniba.di.sss1415.consulenze.activities.NuovaRichiesta1;
import it.uniba.di.sss1415.consulenze.adapters.DispInsAdapter;
import it.uniba.di.sss1415.consulenze.adapters.DisponibilitaInseriteAdapter;
import it.uniba.di.sss1415.consulenze.database.DisponibilitaDB;
import it.uniba.di.sss1415.consulenze.entities.Disponibilita;

/**
 * Fragment per la visualizzazione delle disponibilità inserite
 * By Ruggiero Dibenedetto
 */
public class DisponibilitaInseriteFragment extends com.blunderer.materialdesignlibrary.fragments.ScrollViewFragment {

    DispInsAdapter adapter;
    RecyclerView rv;
    ArrayList<Disponibilita> dispList;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MenuPrincipale.count = 0;
        MenuPrincipale.lastSelect = 1;
        MenuPrincipale.count=0;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getView().getContext());
        String email = sharedPreferences.getString("Email", "ciao@afa.it");
        DisponibilitaDB disponibilitaDB = new DisponibilitaDB(getString(R.string.serverQuery));
        disponibilitaDB.inviaRichiestaLettura();
        while(disponibilitaDB.parsingComplete);
        dispList = disponibilitaDB.getdispList();
        pulisciLista();
        rv = (RecyclerView) getView().findViewById(R.id.cardListDispIns);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity().getApplicationContext());
        rv.setLayoutManager(llm);
        adapter = new DispInsAdapter(dispList);
        rv.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            DisponibilitaDB disponibilitaDB = new DisponibilitaDB(getString(R.string.serverQuery));
            disponibilitaDB.inviaRichiestaLettura();
            while(disponibilitaDB.parsingComplete);
            dispList = disponibilitaDB.getdispList();
            pulisciLista();
            rv = (RecyclerView) getView().findViewById(R.id.cardListDispIns);
            LinearLayoutManager llm = new LinearLayoutManager(getActivity().getApplicationContext());
            rv.setLayoutManager(llm);
            adapter = new DispInsAdapter(dispList);
            rv.setAdapter(adapter);

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_disponibilita_inserite, menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_nuova_disp) {
            Intent intent = new Intent(getView().getContext(), NuovaDisponibilita.class);
            startActivityForResult(intent, 11);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_disp_ins, container, false);
    }

    @Override
    public int getContentView() {
        return R.layout.fragment_disp_ins;
    }

    @Override
    public boolean pullToRefreshEnabled() {
        return false;
    }

    @Override
    public int[] getPullToRefreshColorResources() {
        return new int[0];
    }

    @Override
    public void onRefresh() {

    }

    private void pulisciLista() {
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);

        /*elimino disponibilita passate*/
        for (int i = 0; i < dispList.size(); i++){
            if (Integer.parseInt(dispList.get(i).getData().substring(0,4)) < year){
                dispList.remove(i);
                i = i-1;
            }

            else{
                if (Integer.parseInt(dispList.get(i).getData().substring(0,4)) == year){
                    if (Integer.parseInt(dispList.get(i).getData().substring(5,7)) < month){
                        dispList.remove(i);
                        i = i-1;
                    }
                    else{
                        if (Integer.parseInt(dispList.get(i).getData().substring(5,7)) == month) {
                            if (Integer.parseInt(dispList.get(i).getData().substring(8,10)) < day){
                                dispList.remove(i);
                                i = i-1;
                            }
                        }
                    }
                }
            }
        }

        /*Ordino per data crescente*/
        for (int i=0; i < dispList.size(); i++){

            for (int j = 1; j < dispList.size()-i; j++){
                if(Integer.parseInt(dispList.get(j-1).getData().substring(0,4)) >
                        Integer.parseInt(dispList.get(j).getData().substring(0,4))){
                    Disponibilita temp = dispList.get(j-1);
                    dispList.set(j-1, dispList.get(j));
                    dispList.set(j, temp);
                }
                else{
                    if(Integer.parseInt(dispList.get(j-1).getData().substring(0,4)) ==
                            Integer.parseInt(dispList.get(j).getData().substring(0,4))){
                        if(Integer.parseInt(dispList.get(j-1).getData().substring(5,7)) >
                                Integer.parseInt(dispList.get(j).getData().substring(5,7))){
                            Disponibilita temp = dispList.get(j-1);
                            dispList.set(j-1, dispList.get(j));
                            dispList.set(j, temp);
                        }
                        else{
                            if (Integer.parseInt(dispList.get(j-1).getData().substring(5,7)) ==
                                    Integer.parseInt(dispList.get(j).getData().substring(5,7))){
                                if(Integer.parseInt(dispList.get(j-1).getData().substring(8,10)) >
                                        Integer.parseInt(dispList.get(j).getData().substring(8,10))){
                                    Disponibilita temp = dispList.get(j-1);
                                    dispList.set(j-1, dispList.get(j));
                                    dispList.set(j, temp);
                                }
                            }
                        }
                    }
                }

            }
        }
    }

}

package it.uniba.di.sss1415.consulenze.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.rey.material.widget.Button;

/**
 * Bottoni inseribili all'interno di listView
 */
public class ListButton extends Button {

    public ListButton(Context context, AttributeSet attrs){
        super(context,attrs);
        this.setFocusable(false);
    }

    @Override
    public void setPressed(boolean pressed) {
        if(pressed && ((View) getParent()).isPressed()){
            return;
        }

        super.setPressed(pressed);
    }
}

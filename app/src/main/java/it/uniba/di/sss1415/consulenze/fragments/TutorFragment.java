package it.uniba.di.sss1415.consulenze.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.activities.MenuPrincipale;
import it.uniba.di.sss1415.consulenze.activities.ValutaTutor;
import it.uniba.di.sss1415.consulenze.adapters.TutorAdapter;
import it.uniba.di.sss1415.consulenze.database.TutorDB;

/**
 * Fragment per la visualizzazione dei tutor iscritti al sistema.
 * By Emanuele Pio Barracchia
 */
public class TutorFragment extends com.blunderer.materialdesignlibrary.fragments.ScrollViewFragment {
    RecyclerView rv;
    TutorAdapter tutorAdapter;
    boolean mostraMessaggio = true;

    @Override
    public int getContentView() {
        return R.layout.fragment_tutor;
    }

    @Override
    public boolean pullToRefreshEnabled() {
        return false;
    }

    @Override
    public int[] getPullToRefreshColorResources() {
        return new int[0];
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tutor, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MenuPrincipale.lastSelect = 2;
        MenuPrincipale.count = 0;

        rv = (RecyclerView) getView().findViewById(R.id.cardListTutor);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity().getApplicationContext());
        rv.setLayoutManager(llm);

        TutorDB tutorDB = new TutorDB(getResources().getString(R.string.serverQuery));
        tutorDB.inviaRichiesta();
        while (tutorDB.parsingComplete) ;
        tutorAdapter = new TutorAdapter(tutorDB.getTutor());
        rv.setAdapter(tutorAdapter);

        Toast toast = Toast.makeText(getView().getContext(), "Tappare su un tutor per esprimere una valutazione", Toast.LENGTH_LONG);
        toast.show();

    }


}

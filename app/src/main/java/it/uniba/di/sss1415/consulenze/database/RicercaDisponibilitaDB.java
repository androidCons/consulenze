package it.uniba.di.sss1415.consulenze.database;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Classe per la ricerca delle disponibilita'
 */
public class RicercaDisponibilitaDB {
    private String nome = "";
    private String urlString = null;

    private JSONObject json;

    private static final String TIPO_ELEMENTO="interventiMedici";
    private static final String ACCESSO = "write";

    public volatile boolean parsingComplete = true;
    public RicercaDisponibilitaDB(String url, String specializzazione){
        this.urlString = url;
        json = new JSONObject();
        try {
            json.put("campoMedico", specializzazione);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getName(){
        return nome;
    }

    public String generaParametri(String tipoElemento, String accesso, String jsonDaInviare){
        //parametri = 'accesso:' + accesso + ', elemento:' + this.tipoElemento + ', jsonDaScrivere:' + jsonDaInviare;
        String stringaP;
        stringaP = "accesso:" + accesso + ", elemento:" + tipoElemento + ", jsonDaScrivere:" + jsonDaInviare;
        return stringaP;
    }

    public void inviaRichiesta(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                //NON OSO TOCCARE DA QUA
                try{

                    String urlParameters = generaParametri(TIPO_ELEMENTO, ACCESSO, json.toString());
                    byte[] postData = urlParameters.toString().getBytes("UTF-8");
                    int postDataLength = postData.length;
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    conn.getOutputStream().write(postData);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    String data = convertStreamToString(stream);
                    //A QUA

                    Log.i("RISULTATO " + data, new StringBuilder().toString()); //QUA PUO USCIRE IL JSON O LA STRINGA
                    stream.close();
                    //ASSOCIO ALLA VARIABILE IL RISULTATO DELLA RICHIESTA
                    nome = data;
                    //QUESTO LO METTO QUA PERCHE' NON E' UNA RICHIESTA JSON, ALTRIMENTI VA MESSO
                    //ALLA FINE DEL METODO CHE ELABORA IL JSON
                    parsingComplete = false;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
    static String convertStreamToString(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}

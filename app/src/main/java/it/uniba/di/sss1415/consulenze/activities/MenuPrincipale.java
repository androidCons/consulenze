package it.uniba.di.sss1415.consulenze.activities;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;


import com.blunderer.materialdesignlibrary.handlers.ActionBarDefaultHandler;
import com.blunderer.materialdesignlibrary.handlers.ActionBarHandler;
import com.blunderer.materialdesignlibrary.handlers.NavigationDrawerAccountsHandler;
import com.blunderer.materialdesignlibrary.handlers.NavigationDrawerAccountsMenuHandler;
import com.blunderer.materialdesignlibrary.handlers.NavigationDrawerBottomHandler;
import com.blunderer.materialdesignlibrary.handlers.NavigationDrawerTopHandler;
import com.blunderer.materialdesignlibrary.models.Account;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.fragments.BachecaFragmentCard;
import it.uniba.di.sss1415.consulenze.fragments.DisponibilitaInseriteFragment;
import it.uniba.di.sss1415.consulenze.fragments.TutorFragment;
import it.uniba.di.sss1415.consulenze.fragments.profilo_utente;

/**
 * Menu principale. Activity che permette l'accesso a tutte le principali
 * funzionalita' dell'applicazione.
 * By Emanuele Pio Barracchia
 */
public class MenuPrincipale extends com.blunderer.materialdesignlibrary.activities.NavigationDrawerActivity {
    String[] functionTitles;
    public static int count = 0;
    public static int lastSelect = 0;


    @Override
    public void onBackPressed() {

        if (count == 0) {
            Toast toast = Toast.makeText(this, "Premere di nuovo per uscire", Toast.LENGTH_SHORT);
            toast.show();
            count = 1;
        } else if (count == 1) {
            finish();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public NavigationDrawerAccountsHandler getNavigationDrawerAccountsHandler() {
        return null;
    }

    @Override
    public NavigationDrawerAccountsMenuHandler getNavigationDrawerAccountsMenuHandler() {
        return null;
    }

    @Override
    public void onNavigationDrawerAccountChange(Account account) {
    }

    @Override
    public NavigationDrawerTopHandler getNavigationDrawerTopHandler() {
        return new NavigationDrawerTopHandler(this)
                .addItem(R.string.title_activity_bacheca, new BachecaFragmentCard())
                .addItem(R.string.title_activity_disponibilita_inserite, new DisponibilitaInseriteFragment())
                .addItem(getString(R.string.tutor), new TutorFragment())
                .addItem(getString(R.string.title_activity_gestione_dati_account), new profilo_utente());


    }

    @Override
    public NavigationDrawerBottomHandler getNavigationDrawerBottomHandler() {
        return new NavigationDrawerBottomHandler(this)

                .addItem(R.string.logout, new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), accediSistema.class);
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        sharedPreferences.edit().clear().commit();
                        intent.putExtra(getString(R.string.logout), true);
                        startActivity(intent);
                        finish();
                    }
                });
    }


    @Override
    public boolean overlayActionBar() {
        return false;
    }

    @Override
    public boolean replaceActionBarTitleByNavigationDrawerItemTitle() {
        return true;
    }

    @Override
    public int defaultNavigationDrawerItemSelectedPosition() {
        return lastSelect;
    }

    @Override
    protected ActionBarHandler getActionBarHandler() {
        return new ActionBarDefaultHandler(this);
    }


}



package it.uniba.di.sss1415.consulenze.database;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.entities.Richiesta;

/**
 * Classe che si occupa della connesione al server per la gestione degli appuntamenti.
 * By Emanuele Pio Barracchia
 */
public class RichiestePervenuteDB {
    //per la gestione della connessione
    private static final String TIPO_ELEMENTO = "richiesteValutare";
    private static final String ACCESSO_LETTURA = "read";
    private static final String ACCESSO_CANCELLAZIONE = "write";

    private JSONArray richPervArray;
    ArrayList<Richiesta> richList = new ArrayList<Richiesta>();
    private JSONObject oggettoJson;
    private JSONObject json;

    private String urlString = null;

    public volatile boolean parsingComplete = true;

    public RichiestePervenuteDB(String url) {
        this.urlString = url;
    }

    public RichiestePervenuteDB(String url, String data, String oraInizio, String oraFine, String intervento, String dottore) {
        this.urlString = url;
        json = new JSONObject();
        try {
            json.put("data", data);
            json.put("oraInizio", oraInizio);
            json.put("oraFine", oraFine);
            json.put("intervento", intervento);
            json.put("dottore", dottore);

            Log.i("JSON" + json.toString(), new StringBuilder().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Richiesta> getRichPerv() {
        return richList;
    }

    public String generaParametri(String tipoElemento, String accesso, String jsonDaInviare) {
        //parametri = 'accesso:' + accesso + ', elemento:' + this.tipoElemento + ', jsonDaScrivere:' + jsonDaInviare;
        String stringaP;
        stringaP = "accesso:" + accesso + ", elemento:" + tipoElemento + ", jsonDaScrivere:" + jsonDaInviare;
        return stringaP;
    }

    public void inviaRichiestaLettura() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    String urlParameters = generaParametri(TIPO_ELEMENTO, ACCESSO_LETTURA, "");
                    byte[] postData = urlParameters.toString().getBytes("UTF-8");
                    int postDataLength = postData.length;
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    conn.getOutputStream().write(postData);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    String data = convertStreamToString(stream);
                    Log.i("RISULTATO " + data, new StringBuilder().toString());
                    elaboraJson(data);
                    stream.close();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

    }

    //NON FUNZIONA
    public void inviaEliminaRichiesta() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    String urlParameters = generaParametri(TIPO_ELEMENTO, ACCESSO_CANCELLAZIONE, json.toString());
                    byte[] postData = urlParameters.toString().getBytes("UTF-8");
                    int postDataLength = postData.length;
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    conn.getOutputStream().write(postData);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    String data = convertStreamToString(stream);

                    Log.i("RISULTATO " + data, new StringBuilder().toString());
                    parsingComplete = false;
                    stream.close();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

    }

    public void elaboraJson(String result) {
        oggettoJson = null;
        try {
            oggettoJson = new JSONObject(result);
            System.out.println("JSON = " + oggettoJson.toString());

            richPervArray = oggettoJson.getJSONArray("richieste");

            for (int i = 0; i < richPervArray.length(); i++) {
                JSONObject rich = richPervArray.getJSONObject(i);
                richList.add(new Richiesta(
                        "", //campo id
                        rich.getString("data").substring(0, 10),
                        rich.getString("oraInizio"),
                        rich.getString("oraFine"),
                        rich.getString("intervento"),
                        rich.getString("dottore")
                ));


            }
            parsingComplete = false;
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}

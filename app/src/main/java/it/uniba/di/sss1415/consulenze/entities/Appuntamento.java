package it.uniba.di.sss1415.consulenze.entities;


public class Appuntamento {
  private String tipo;
  private String data;
  private String oraInizio;
  private String oraFine;
  private String intervento;
  private String medico;

  public Appuntamento(String tipo, String data, String oraInizio, String oraFine, String intervento, String medico){
    super();
    this.tipo = tipo;
    this.data = data;
    this.oraInizio = oraInizio;
    this.oraFine = oraFine;
    this.intervento = intervento;
    this.medico = medico;
  }

  public String getTipo(){
    return tipo;
  }


  public String getData(){
    return data;
  }

  public String getOraInizio(){
    return oraInizio;
  }

  public String getOraFine(){
    return oraFine;
  }

  public String getIntervento(){
    return intervento;
  }

  public String getMedico(){
    return medico;
  }
}

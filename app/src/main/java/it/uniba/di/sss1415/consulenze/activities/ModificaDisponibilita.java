package it.uniba.di.sss1415.consulenze.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Calendar;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.database.DisponibilitaDB;
import it.uniba.di.sss1415.consulenze.other.Utility;

/**
 * Classe per modificare una disponibilita' inserita
 */
public class ModificaDisponibilita extends ActionBarActivity {

    private String id;
    private String data;
    private String ora_inizio;
    private String ora_fine;
    private String intervento;
    private String ripetizione;
    private String fineRipetizione;
    private int startDay;
    private int startMonth;
    private int startYear;
    private int startMinuteTime;
    private int startHourTime;
    private int endDay;
    private int endMonth;
    private int endYear;
    private int endMinuteTime;
    private int endHourTime;
    private boolean confermaModifica = false;
    private boolean confermaCancella = false;
    private Boolean controlloStartDate = true;
    private Boolean controlloEndDate = true;
    private Boolean controlloEndTime = true;
    private Boolean controlloStartTime = true;
    private Toast toast;

    Calendar c = Calendar.getInstance();
    private int todayDay = c.get(Calendar.DAY_OF_MONTH);
    private int todayMonth = c.get(Calendar.MONTH);
    private int todayYear = c.get(Calendar.YEAR);

    private TextView txtData;
    private TextView txtEndDate;
    private TextView txtStartTime;
    private TextView txtEndTime;
    private TextView txtDescr;
    private ToggleButton toggleButton;
    private RadioGroup radioGroup;

    /*------ Costanti per Dialog Case ------*/
    static final int SET_START_DATE = 0;
    static final int SET_END_DATE = 1;
    static final int SET_START_TIME = 2;
    static final int SET_END_TIME = 3;

    public DatePickerDialog.OnDateSetListener startDatePickerListener =
            new DatePickerDialog.OnDateSetListener() {

                // when dialog box is closed, below method will be called.
                public void onDateSet(DatePicker view, int selectedYear,
                                      int selectedMonth, int selectedDay) {
                    controlloStartDate = true;

                    if(selectedYear < todayYear){
                        controlloStartDate = false;
                    }else{
                        if(selectedYear == todayYear){
                            if(selectedMonth < todayMonth){
                                controlloStartDate = false;
                            }else{
                                if (selectedMonth == todayMonth){
                                    if (selectedDay < todayDay){
                                        controlloStartDate = false;
                                    }
                                }
                            }
                        }
                    }

                    if(controlloStartDate){
                        startYear = selectedYear;
                        startMonth = selectedMonth + 1;
                        startDay = selectedDay;
                        txtData.setText("Data: " + Utility.formattaDataOra(startDay) + "/" + Utility.formattaDataOra(startMonth) + "/" + startYear);
                    }else{
                        toast = Toast.makeText(getApplicationContext(), "La data deve essere maggiore" +
                                " o uguale a quella di oggi", Toast.LENGTH_SHORT);
                        toast.show();
                        txtData.setText(R.string.inserisci_data);
                    }
                }
            };

    public DatePickerDialog.OnDateSetListener endDatePickerListener =
            new DatePickerDialog.OnDateSetListener() {

                // when dialog box is closed, below method will be called.
                public void onDateSet(DatePicker view, int selectedYear,
                                      int selectedMonth, int selectedDay) {
                    controlloEndDate = true;
                    if(selectedYear < startYear){
                        controlloEndDate = false;
                    }else{
                        if(selectedYear == startYear){
                            if(selectedMonth + 1  < startMonth){
                                controlloEndDate = false;
                            }else{
                                if (selectedMonth + 1 == startMonth){
                                    if (selectedDay < startDay){
                                        controlloEndDate = false;
                                    }
                                }
                            }
                        }
                    }

                    if(controlloEndDate){
                        endYear = selectedYear;
                        endMonth = selectedMonth + 1;
                        endDay = selectedDay;
                        txtEndDate.setText("Data: " + Utility.formattaDataOra(endDay) + "/" + Utility.formattaDataOra(endMonth) + "/" + endYear);
                    }else{
                        toast = Toast.makeText(getApplicationContext(), "La data di fine deve essere maggiore" +
                                " o uguale a quella di inizio", Toast.LENGTH_SHORT);
                        toast.show();
                        txtEndDate.setText("Iposta data scadenza");
                    }
                }
            };


    /*-------Inizio dichiarazioni variabili per Data Picker -------*/

    public TimePickerDialog.OnTimeSetListener startTimePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    controlloStartTime = true;

                    startMinuteTime = minute;
                    startHourTime = hourOfDay;

                    txtStartTime.setText("Orario inizio: " + Utility.formattaDataOra(startHourTime) + ":" + Utility.formattaDataOra(startMinuteTime));
                }
            };

    public TimePickerDialog.OnTimeSetListener endTimePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    controlloEndTime = true;
                    if(hourOfDay < startHourTime){
                        controlloEndTime = false;
                    }else{
                        if (hourOfDay == startHourTime){
                            if (minute <= startMinuteTime){
                                controlloEndTime = false;
                            }
                        }
                    }
                    if(controlloEndTime){
                        endMinuteTime = minute;
                        endHourTime = hourOfDay;

                        txtEndTime.setText("Orario Fine: " + Utility.formattaDataOra(endHourTime) +
                                ":" + Utility.formattaDataOra(endMinuteTime));
                    }else{
                        toast = Toast.makeText(getApplicationContext(), "L'orario di fine deve essere " +
                                "maggiore a quella di inizio", Toast.LENGTH_SHORT);
                        toast.show();
                        txtEndTime.setText("Imposta ora fine");
                    }
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifica_disponibilita);

        txtData = (TextView) findViewById(R.id.textViewData);
        txtEndTime = (TextView) findViewById(R.id.textViewEndTime);
        txtStartTime = (TextView) findViewById(R.id.textViewStartTime);
        txtEndDate = (TextView) findViewById(R.id.textViewEndDate);

        /*recupero valori dall'intent*/
        id = getIntent().getExtras().getString("id");
        data = getIntent().getExtras().getString("data");
        ora_inizio = getIntent().getExtras().getString("ora_inizio");
        ora_fine = getIntent().getExtras().getString("ora_fine");
        intervento = getIntent().getExtras().getString("intervento");
        ripetizione = getIntent().getExtras().getString("ripetizione");
        fineRipetizione = getIntent().getExtras().getString("fineRipetizione");

        /*Recupero Valori Date e orari*/
        startYear = Integer.parseInt(data.substring(0, 4));
        startMonth = Integer.parseInt(data.substring(5, 7));
        startDay = Integer.parseInt(data.substring(8, 10));
        endYear = Integer.parseInt(fineRipetizione.substring(0, 4));
        endMonth = Integer.parseInt(fineRipetizione.substring(5, 7));
        endDay = Integer.parseInt(fineRipetizione.substring(8, 10));
        startHourTime = Integer.parseInt(ora_inizio.substring(0, 2));
        startMinuteTime = Integer.parseInt(ora_inizio.substring(3,5));
        endHourTime = Integer.parseInt(ora_fine.substring(0,2));
        endMinuteTime = Integer.parseInt(ora_fine.substring(3,5));

        ImageButton imgBtnStartDate = (ImageButton) findViewById(R.id.imgBtnCalendar);
        imgBtnStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_START_DATE).show();
            }
        });

        txtData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_START_DATE).show();
            }
        });

        ImageButton imgBtnEndDate = (ImageButton) findViewById(R.id.imgBtnEndDate);
        imgBtnEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_END_DATE).show();
            }
        });

        txtEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_END_DATE).show();
            }
        });

        ImageButton imgBtnStartTime = (ImageButton) findViewById(R.id.imgBtnStartTime);
        imgBtnStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_START_TIME).show();
            }
        });

        txtStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_START_TIME).show();
            }
        });

        ImageButton imgBtnEndTime = (ImageButton) findViewById(R.id.imgBtnEndTime);
        imgBtnEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_END_TIME).show();
            }
        });

        txtEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_END_TIME).show();
            }
        });

        /*Popolamento campi con dati provenienti dall'intent*/
        txtDescr = (TextView) findViewById(R.id.textViewDescription);
        txtDescr.setText("Stai modificando una disponibilita' di tutoraggio per: " + intervento);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        txtData.setText("Data: " + data.substring(8) + "/" + data.substring(5, 7) + "/" + data.substring(0, 4));
        txtStartTime.setText("Orario inizio: " + ora_inizio.substring(0, 5));
        txtEndTime.setText("Orario fine: " + ora_fine.substring(0, 5));
        toggleButton = (ToggleButton) findViewById(R.id.ToggleButtonRipetizione);
        if (!ripetizione.equals("")){
            toggleButton.setEnabled(true);
            radioGroup.setVisibility(View.VISIBLE);
            if (ripetizione.equals("Ogni settimana")){
                RadioButton oneWeek = (RadioButton) findViewById(R.id.radioButtonEveryWeek);
                oneWeek.setEnabled(true);
            } else {
                RadioButton twoWeek = (RadioButton) findViewById(R.id.radioButton2Week);
                twoWeek.setEnabled(true);
            }

            imgBtnEndDate = (ImageButton) findViewById(R.id.imgBtnEndDate);
            imgBtnEndDate.setVisibility(View.VISIBLE);

            txtEndDate.setVisibility(View.VISIBLE);
            txtEndDate.setText("Fino al:" + Utility.formattaDataOra(endDay) + "/" + Utility.formattaDataOra(endMonth) + "/" + endYear);
        }




        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean on = ((ToggleButton) v).isChecked();
                RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

                if (on) {
                    radioGroup.setVisibility(View.VISIBLE);
                    ImageButton imgbtnEndDate = (ImageButton) findViewById(R.id.imgBtnEndDate);
                    imgbtnEndDate.setVisibility(View.VISIBLE);
                    TextView endDate = (TextView) findViewById(R.id.textViewEndDate);
                    endDate.setVisibility(View.VISIBLE);
                    controlloEndDate = false;
                } else {
                    radioGroup.setVisibility(View.INVISIBLE);
                    ImageButton imgbtnEndDate = (ImageButton) findViewById(R.id.imgBtnEndDate);
                    imgbtnEndDate.setVisibility(View.INVISIBLE);
                    TextView endDate = (TextView) findViewById(R.id.textViewEndDate);
                    endDate.setVisibility(View.INVISIBLE);
                    ripetizione = "";
                    controlloEndDate = true;
                }

            }
        });

        if(savedInstanceState != null){
            txtData.setText(savedInstanceState.getString("Data"));
            txtDescr.setText(savedInstanceState.getString("Descr"));
            txtStartTime.setText(savedInstanceState.getString("StartTime"));
            txtEndTime.setText(savedInstanceState.getString("EndTime"));
            if (savedInstanceState.getBoolean("RipetEnabled") == true){
                toggleButton.setChecked(true);
                radioGroup.setVisibility(View.VISIBLE);
                radioGroup.check(savedInstanceState.getInt("Ripet"));
                txtEndDate.setText(savedInstanceState.getString("EndDate"));
                txtEndDate.setVisibility(View.VISIBLE);
                imgBtnEndDate.setVisibility(View.VISIBLE);

            } else {
                toggleButton.setChecked(false);
                radioGroup.setVisibility(View.INVISIBLE);
            }
        }

        final Button btnModifica = (Button) findViewById(R.id.btnModifica);
        final Button btnCancella = (Button) findViewById(R.id.btnCancella);

        btnModifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (controlloEndTime && controlloStartDate && controlloEndDate && controlloStartTime) {
                    if (!confermaModifica) {
                        confermaCancella = false;
                        btnCancella.setTextColor(getResources().getColor(R.color.white));
                        btnCancella.setBackgroundColor(getResources().getColor(R.color.accent));

                        btnModifica.setBackgroundColor(getResources().getColor(R.color.confirm_button));
                        btnModifica.setTextColor(getResources().getColor(R.color.carbon_black));
                        confermaModifica = true;
                        Toast.makeText(getApplicationContext(), getString(R.string.confirm_click), Toast.LENGTH_SHORT).show();
                    } else {
                        confermaModifica = false;
                        btnModifica.setBackgroundColor(getResources().getColor(R.color.button_material_light));
                        btnCancella.setBackgroundColor(getResources().getColor(R.color.button_material_light));
                        modificaDisponibilita();
                        setResult(RESULT_OK);
                        finish();
                    }
                }else{
                    toast = Toast.makeText(getApplicationContext(), "Si e' verificato un errore, ricontrollare" +
                            " i campi", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });


        btnCancella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!confermaCancella){
                    confermaModifica = false;
                    btnModifica.setTextColor(getResources().getColor(R.color.white));
                    btnModifica.setBackgroundColor(getResources().getColor(R.color.accent));

                    btnCancella.setBackgroundColor(getResources().getColor(R.color.confirm_button));
                    btnCancella.setTextColor(getResources().getColor(R.color.carbon_black));
                    confermaCancella=true;
                    Toast.makeText(getApplicationContext(), getString(R.string.confirm_click), Toast.LENGTH_SHORT).show();
                }
                else {
                    confermaCancella=false;
                    btnModifica.setBackgroundColor(getResources().getColor(R.color.button_material_light));
                    btnCancella.setBackgroundColor(getResources().getColor(R.color.button_material_light));
                    cancellaDisponibilita();
                    setResult(RESULT_OK);
                    finish();
                }
            }
        });


        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.rlModDisp);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCancella.setTextColor(getResources().getColor(R.color.white));
                btnModifica.setTextColor(getResources().getColor(R.color.white));

                confermaCancella = false;
                confermaModifica = false;

                btnCancella.setBackgroundColor(getResources().getColor(R.color.accent));
                btnModifica.setBackgroundColor(getResources().getColor(R.color.accent));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_modifica_disponibilita, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /*-------Date Picker--------------*/
    private Dialog createdDialog(int id) {
        switch (id) {
            case (SET_START_DATE): {
                DatePickerDialog startDatePickerDialog = new DatePickerDialog(this,
                        startDatePickerListener, startYear, startMonth-1, startDay);
                return startDatePickerDialog;
            }
            case (SET_END_DATE): {
                DatePickerDialog endDatePickerDialog = new DatePickerDialog(this,
                        endDatePickerListener, startYear, startMonth-1, startDay);
                return endDatePickerDialog;
            }
            case (SET_START_TIME): {
                TimePickerDialog startTimePickerDialog = new TimePickerDialog(this,
                        startTimePickerListener, startHourTime, startMinuteTime, true);
                return startTimePickerDialog;
            }
            case (SET_END_TIME): {
                TimePickerDialog endTimePickerDialog = new TimePickerDialog(this,
                        endTimePickerListener, endHourTime, endMinuteTime, true);
                return endTimePickerDialog;
            }
        }
        return null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("Data", txtData.getText().toString());
        outState.putString("Descr", txtDescr.getText().toString());
        outState.putString("StartTime", txtStartTime.getText().toString());
        outState.putString("EndTime", txtEndTime.getText().toString());
        outState.putBoolean("RipetEnabled", toggleButton.isChecked());
        outState.putInt("Ripet", radioGroup.getCheckedRadioButtonId());
        outState.putString("EndDate", txtEndDate.getText().toString());
    }

    private void modificaDisponibilita(){

        String data = startYear + "-" + startMonth + "-" + startDay;
        String ora_inizio = startHourTime + ":" + startMinuteTime;
        String ora_fine = endHourTime + ":" + endMinuteTime;

        DisponibilitaDB obj = new DisponibilitaDB(getString(R.string.serverQuery), id, data, ora_inizio, ora_fine, intervento);
        obj.inviaRichiestaModifica();
    }

    private void cancellaDisponibilita(){
        DisponibilitaDB obj = new DisponibilitaDB(getString(R.string.serverQuery), id, data, ora_inizio, ora_fine, intervento);
        obj.inviaRichiestaCancella();
    }


}

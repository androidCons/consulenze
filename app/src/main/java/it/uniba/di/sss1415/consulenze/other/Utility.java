package it.uniba.di.sss1415.consulenze.other;

import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.entities.Appuntamento;
import it.uniba.di.sss1415.consulenze.entities.Richiesta;

/**
 * Classe contenente varie utility.
 */
public class Utility {

  public static void ordinaAppunt(ArrayList<Appuntamento> app){
    Collections.sort(app, new Comparator<Appuntamento>() {


      @Override
      public int compare(Appuntamento lhs, Appuntamento rhs) {
        if (lhs.getData().equals(rhs.getData())){
          return lhs.getOraInizio().compareTo(rhs.getOraInizio());
        } else {
          return lhs.getData().compareTo(rhs.getData());
        }
      }
    });
  }

  public static void ordinaRichiesta(ArrayList<Richiesta> rich){
    Collections.sort(rich, new Comparator<Richiesta>() {
      @Override
      public int compare(Richiesta lhs, Richiesta rhs) {
        if (lhs.getData().equals(rhs.getData())){
          return lhs.getOraInizio().compareTo(rhs.getOraInizio());
        } else {
          return lhs.getData().compareTo(rhs.getData());
        }
      }
    });
  }

  public static String trasformaData(String data, Context c){
    String anno = data.substring(0,4);
    int mese = Integer.parseInt(data.substring(5, 7));
    String meseS;
    String giorno = data.substring(8);
    String[] mesi = c.getResources().getStringArray(R.array.mesi);
    meseS = mesi[mese-1];
    return giorno + " " + meseS + " " + anno;


  }

  public static String formattaDataOra(int num){
    if (num < 10){
      return "0"+ num;
    }
    return "" + num;
  }
}

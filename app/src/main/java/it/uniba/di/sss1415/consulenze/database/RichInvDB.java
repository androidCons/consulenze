package it.uniba.di.sss1415.consulenze.database;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.entities.Richiesta;

/**
 * Classe per la connessione al server per la gestione delle richieste inviate.
 * By Emanuele Pio Barracchia
 */
public class RichInvDB {

  private JSONArray richInvArray;
  private  ArrayList<Richiesta> richInv = new ArrayList<Richiesta>();
  private JSONObject oggettoJson;
    private JSONObject json;

  private String urlString = null;

  private static final String TIPO_ELEMENTO="mieRichiesteInserite";
  private static final String ACCESSO_READ = "read";
  private static final String ACCESSO_WRITE = "write";
    private static final String ACCESSO_CHANGE = "change";
    private static final String ACCESSO_DELETE = "delete";

  public volatile boolean parsingComplete = true;

  public RichInvDB(String url){
    this.urlString = url;
  }

    public RichInvDB(String url, String id, String data, String oraInizio, String oraFine, String intervento, String email) {
        this.urlString = url;
        json = new JSONObject();
        try{
            json.put("id", id);
            json.put("data", data);
            json.put("oraInizio", oraInizio);
            json.put("oraFine", oraFine);
            json.put("intervento", intervento);
            json.put("nomeTutor", "");
            json.put("cognomeTutor", "");
            json.put("percorso", "1");
            json.put("utente", email);



        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }

    public ArrayList<Richiesta> getRichInv(){
    return richInv;
  }

  public String generaParametri(String tipoElemento, String accesso, String jsonDaInviare){
    //parametri = 'accesso:' + accesso + ', elemento:' + this.tipoElemento + ', jsonDaScrivere:' + jsonDaInviare;
    String stringaP;
    stringaP = "accesso:" + accesso + ", elemento:" + tipoElemento + ", jsonDaScrivere:" + jsonDaInviare;
    return stringaP;
  }

  public void inviaRichiesta(){
    Thread thread = new Thread(new Runnable() {
      @Override
      public void run() {
        try{

          String urlParameters = generaParametri(TIPO_ELEMENTO, ACCESSO_READ, "");
          byte[] postData = urlParameters.toString().getBytes("UTF-8");
          int postDataLength = postData.length;
          URL url = new URL(urlString);
          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
          conn.setReadTimeout(10000);
          conn.setConnectTimeout(15000);
          conn.setRequestMethod("POST");
          conn.setDoInput(true);
          conn.setDoOutput(true);
          conn.setInstanceFollowRedirects(false);
          conn.setRequestProperty("Content-Type", "application/json");
          conn.setRequestProperty("Accept", "application/json");
          conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
          conn.setUseCaches(false);
          conn.getOutputStream().write(postData);
          conn.connect();
          InputStream stream = conn.getInputStream();
          String data = convertStreamToString(stream);

          Log.i("RISULTATO " + data, new StringBuilder().toString());
          elaboraJson(data);
          stream.close();

        } catch (MalformedURLException e) {
          e.printStackTrace();
        } catch (ProtocolException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    });
    thread.start();
  }


    public void inviaRichiestaScrittura(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{

                    String parametri = generaParametri(TIPO_ELEMENTO,ACCESSO_WRITE,json.toString());
                    System.out.println(parametri);
                    byte[] postData = parametri.toString().getBytes("UTF-8");
                    int postDataLength = postData.length;
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    conn.getOutputStream().write(postData);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    String data = convertStreamToString(stream);

                    Log.i("RISULTATO " + data, new StringBuilder().toString());
                    stream.close();
                    Log.i("STRINGA RIC = ", data.toString());
                    parsingComplete = false;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public void inviaRichiestaModifica(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{

                    String parametri = generaParametri(TIPO_ELEMENTO,ACCESSO_CHANGE,json.toString());
                    System.out.println(parametri);
                    byte[] postData = parametri.toString().getBytes("UTF-8");
                    int postDataLength = postData.length;
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    conn.getOutputStream().write(postData);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    String data = convertStreamToString(stream);

                    Log.i("RISULTATO " + data, new StringBuilder().toString());
                    stream.close();
                    Log.i("STRINGA RIC = ", data.toString());
                    parsingComplete = false;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public void inviaRichiestaCancella(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{

                    String parametri = generaParametri(TIPO_ELEMENTO,ACCESSO_DELETE,json.toString());
                    System.out.println(parametri);
                    byte[] postData = parametri.toString().getBytes("UTF-8");
                    int postDataLength = postData.length;
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestProperty("Content-Type", "application/json");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                    conn.setUseCaches(false);
                    conn.getOutputStream().write(postData);
                    conn.connect();
                    InputStream stream = conn.getInputStream();
                    String data = convertStreamToString(stream);
                    //A QUA

                    Log.i("RISULTATO " + data, new StringBuilder().toString());
                    stream.close();
                    Log.i("STRINGA RIC = ", data.toString());
                    parsingComplete = false;
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

  public void elaboraJson(String result){
    oggettoJson = null;
    try {
      oggettoJson = new JSONObject(result);
      System.out.println("JSON = " + oggettoJson.toString());

      richInvArray = oggettoJson.getJSONArray("richiesteUtente");

      for (int i = 0; i<richInvArray.length(); i++){
        JSONObject rich = richInvArray.getJSONObject(i);
        richInv.add(new Richiesta(
                rich.getString("id"),
                rich.getString("data").substring(0,10),
                rich.getString("oraInizio"),
                rich.getString("oraFine"),
                rich.getString("intervento"),
                rich.getString("nomeTutor") + " " + rich.getString("cognomeTutor")
        ));


      }
      parsingComplete = false;
    } catch (JSONException e) {
      e.printStackTrace();
    }

  }


  static String convertStreamToString(java.io.InputStream is) {
    java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
    return s.hasNext() ? s.next() : "";
  }

  public String lastId() {
    return richInv.get(richInv.size()-1).getId();
  }
}

package it.uniba.di.sss1415.consulenze.entities;

import org.json.JSONObject;


public class Disponibilita {
    private String id;
    private String data;
    private String oraInizio;
    private String oraFine;
    private String intervento;
    private String ripetizione;
    private String fineRipetizione;

    public Disponibilita(String id, String data,String oraInizio,String oraFine,String intervento,String ripetizione, String fineRipetizione){
        super();
        this.id = id;
        this.data = data;
        this.oraInizio = oraInizio;
        this.oraFine = oraFine;
        this.intervento = intervento;
        this.ripetizione = ripetizione;
        this.fineRipetizione = fineRipetizione;
    }

    public String getId() {return id; }

    public String getData(){
        return data;
    }

    public String getOraInizio(){
        return oraInizio;
    }

    public String getOraFine(){
        return oraFine;
    }

    public String getIntervento(){
        return intervento;
    }

    public String getRipetizione(){
        return ripetizione;
    }

    public String getFineRipetizione() {
        return fineRipetizione;
    }


}

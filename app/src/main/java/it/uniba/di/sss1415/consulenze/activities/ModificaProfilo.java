package it.uniba.di.sss1415.consulenze.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.blunderer.materialdesignlibrary.activities.Activity;
import com.blunderer.materialdesignlibrary.handlers.ActionBarDefaultHandler;
import com.blunderer.materialdesignlibrary.handlers.ActionBarHandler;
import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;
import com.rey.material.widget.Spinner;
import com.rey.material.widget.TextView;

import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.database.BrancheMedicheDB;
import it.uniba.di.sss1415.consulenze.database.ModificaProfiloDB;
import it.uniba.di.sss1415.consulenze.entities.UserData;

/**
 * Activity che si occupa della modifica dei dati del profilo personale.
 * By Fabrizio Centrone
 */
public class ModificaProfilo extends AppCompatActivity {

    //campi di testo, spinner, bottoni
    private boolean login=false;
    private TextView ETemail;
    private EditText ETnome, ETcognome, ETanno, ETnumero;
    private Spinner lista_province;
    private Spinner lista_branche_mediche;
    private Button btnupdate;
    private String password="";
    private String spec="";
    private ArrayList<String> query_spec;
    ArrayAdapter<CharSequence> adapter;
    ArrayAdapter<String> med_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifica_profilo);

        //inizializza campi
        ETemail= (TextView) findViewById(R.id.etModMail);
        ETnome= (EditText) findViewById(R.id.etModNome);
        ETcognome= (EditText) findViewById(R.id.etModCognome);
        ETanno= (EditText) findViewById(R.id.etModAnno);
        ETnumero= (EditText) findViewById(R.id.etModNumero);

        // inizializzazione spinner province
        lista_province = (Spinner) findViewById(R.id.spinnerModProvince);
        adapter = ArrayAdapter.createFromResource(this, R.array.arrayProvince,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lista_province.setAdapter(adapter);

        // inizializzazione spinner branche mediche
        lista_branche_mediche = (Spinner) findViewById(R.id.spinnerModSpec);
        if (isOnline()) {
            query_spec = queryBrancheMediche();
            med_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, query_spec);
            med_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            lista_branche_mediche.setAdapter(med_adapter);
        } else {
            // errore connessione internet
            Toast.makeText(getApplicationContext(), getString(R.string.no_conn), Toast.LENGTH_LONG).show();
            spec="";
        }

        // prendi dati utente e popola campi
        prendi_dati_utente();

        //listener dei bottoni
        btnupdate = (Button) findViewById(R.id.btnConfermaModifiche);
        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modifica_profilo();
            }
        });

        //ripristino stato precedente
        if (savedInstanceState!=null){
            ETnome.setText(savedInstanceState.getString(getString(R.string.nome),""));
            ETcognome.setText(savedInstanceState.getString(getString(R.string.cognome),""));
            ETanno.setText(savedInstanceState.getString(getString(R.string.anno),""));
            ETnumero.setText(savedInstanceState.getString(getString(R.string.numero),""));
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outstate){
        super.onSaveInstanceState(outstate);
        outstate.putString(getString(R.string.nome),ETnome.getText().toString());
        outstate.putString(getString(R.string.cognome),ETcognome.getText().toString());
        outstate.putString(getString(R.string.anno),ETanno.getText().toString());
        outstate.putString(getString(R.string.numero),ETnumero.getText().toString());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_modifica_profilo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    /**
     * Metodo che ottiene dal server la lista delle specializzazioni mediche da visualizzare
     * nell'apposito spinner.
     * @return la lista delle branche mediche se la connessione e' avvenuta con successo,
     * lista vuota altrimenti
     */
    private ArrayList<String> queryBrancheMediche(){

        ArrayList<String> list = new ArrayList<String>();
        if(isOnline()) {
            BrancheMedicheDB obj = new BrancheMedicheDB(getString(R.string.serverQuery));
            obj.inviaRichiesta();
            while(obj.parsingComplete);
            String lista= obj.getName();

            char c;
            String temp="";
            for (int i=0; i<lista.length();i++){
                c=lista.charAt(i);
                if (c==','|| i==lista.length()){
                    list.add(temp);
                    temp="";
                } else temp = temp + c;
            }
        } else {
            // errore connessione internet
            Toast.makeText(getApplicationContext(), getString(R.string.no_conn), Toast.LENGTH_LONG).show();
        }
        return list;

    }

    /**
     * Metodo richiamato in fase di modifica dei dati personali
     */
    private void modifica_profilo(){

        ETnome.clearError();
        ETcognome.clearError();
        ETanno.clearError();
        ETnumero.clearError();

        String url, mail, nome, cognome, prov, anno, numero;
        url= getString(R.string.serverQuery);
        mail=ETemail.getText().toString();
        nome =ETnome.getText().toString();
        cognome =ETcognome.getText().toString();
        prov =lista_province.getSelectedItem().toString();
        anno =ETanno.getText().toString();
        numero =ETnumero.getText().toString();
        // prendi stringa specializzazione
        try{
            spec = lista_branche_mediche.getSelectedItem().toString();
        } catch(Exception e) {spec="";}

        try {
            //controllo campi
            boolean check = controllo_campi(nome, cognome, anno, numero, spec);
            if (check) {
                if(isOnline()) {
                    ModificaProfiloDB mp = new ModificaProfiloDB(url, mail, nome, cognome, prov, anno, numero, spec);
                    mp.inviaRichiesta();
                    while (mp.parsingComplete) ;
                    //controlla se l'operazione e' riuscita o meno
                    if (!(mp.getRisposta()==null || mp.getRisposta().compareTo("")==0)) {
                        //aggiorna dati utente
                        UserData ud = new UserData(mp.getRisposta(),true);
                        aggiorna_dati_utente(ud);

                        //VAI AL MENU PRINCIPALE
                        String message = getApplicationContext().getResources().getString(R.string.profileUpdate);
                        Toast toastMessage = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
                        toastMessage.show();
                        Intent intent = new Intent(ModificaProfilo.this, MenuPrincipale.class);
                        MenuPrincipale.lastSelect=0;
                        startActivity(intent);
                        finish();
                    } else {
                        //errore connessione db
                        Toast errordb = Toast.makeText(getApplicationContext(), getString(R.string.errorUpdate), Toast.LENGTH_LONG);
                        errordb.show();
                    }
                } else {
                    // errore connessione internet
                    Toast.makeText(getApplicationContext(), getString(R.string.no_conn), Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            //errore connessione db
            Toast errordb = Toast.makeText(getApplicationContext(), getString(R.string.errorUpdate), Toast.LENGTH_LONG);
            errordb.show();
        }
    }

    /**
     * Controllo valori inseriti dall'utente
     * @param nome
     * @param cognome
     * @param anno
     * @param numero
     * @param spec
     * @return true se tutti i valori inseriti sono corretti, false altrimenti
     */
    private boolean controllo_campi(String nome, String cognome, String anno, String numero, String spec){
        Context context = getApplicationContext();
        Toast toastMessage;
        String error="";

            if      (nome.compareTo("")==0) {error = getApplicationContext().getResources().getString(R.string.insertNome); ETnome.setError(getString(R.string.insertNome));}
            else if (nome.contains(",")) {error = getApplicationContext().getResources().getString(R.string.invalid_value); ETnome.setError(getString(R.string.invalid_value));}
            else if (cognome.compareTo("")==0) {error = getApplicationContext().getResources().getString(R.string.insertCognome);ETcognome.setError(getString(R.string.insertCognome));}
            else if (cognome.contains(",")) {error = getApplicationContext().getResources().getString(R.string.invalid_value); ETcognome.setError(getString(R.string.invalid_value));}
            else if (anno.compareTo("")==0) {error = getApplicationContext().getResources().getString(R.string.insertAnno);ETanno.setError(getString(R.string.insertAnno));}
            else if (anno.contains(",")) {error = getApplicationContext().getResources().getString(R.string.invalid_value); ETanno.setError(getString(R.string.invalid_value));}
            else if (numero.compareTo("")==0) {error = getApplicationContext().getResources().getString(R.string.insertNumero);ETnumero.setError(getString(R.string.insertNumero));}
            else if (numero.contains(",")) {error = getApplicationContext().getResources().getString(R.string.invalid_value); ETnumero.setError(getString(R.string.invalid_value));}
            else if (spec==null || spec.compareTo("")==0) {error = getApplicationContext().getResources().getString(R.string.no_conn);}
            else return true;

        toastMessage = Toast.makeText(context, error, Toast.LENGTH_LONG);
        toastMessage.show();
        return false;
    }

    /**
     * Raccoglie i dati utente memorizzati sulle shared preferences per visualizzarli negli appositi campi.
     */
    private void prendi_dati_utente(){
        try {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            login = sharedPref.getBoolean(getString(R.string.login), false);
            ETemail.setText(sharedPref.getString(getString(R.string.email), ""));
            ETnome.setText(sharedPref.getString(getString(R.string.nome), ""));
            ETcognome.setText(sharedPref.getString(getString(R.string.cognome), ""));
            ETanno.setText(sharedPref.getString(getString(R.string.anno), ""));
            ETnumero.setText(sharedPref.getString(getString(R.string.numero), ""));
            lista_province.setSelection(adapter.getPosition(sharedPref.getString(getString(R.string.txtProv), "")));

            String value = sharedPref.getString(getString(R.string.spec_prim), "");
            if (!(value.compareTo("")==0)){
                lista_branche_mediche.setSelection(query_spec.lastIndexOf(value));
            }

            password = sharedPref.getString(getString(R.string.password), "");
        } catch (Exception e) {}
    }

    /**
     * Memorizza le modifiche effettuate al profilo sulle shared preferences.
     * @param user contiene tutti i dati utente dopo la modifica del profilo
     */
    private void aggiorna_dati_utente(UserData user){
        try {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.clear();

            editor.putString(getString(R.string.email), user.getMail());
            editor.putString(getString(R.string.password), user.getPass());
            editor.putString(getString(R.string.txtProv), user.getProvincia());
            editor.putString(getString(R.string.numero), user.getNumero());
            editor.putString(getString(R.string.anno), user.getAnno());
            editor.putString(getString(R.string.nome), user.getNome());
            editor.putString(getString(R.string.cognome), user.getCognome());
            editor.putString(getString(R.string.spec_prim), user.getSpecializzazione());
            editor.putBoolean(getString(R.string.login), login);
            editor.putString(getString(R.string.score), sharedPref.getString(getString(R.string.score), ""));
            editor.apply();
        } catch (Exception e) {}
    }

    /**
     * Controlla se la connessione ad internet e' attiva
     * @return true se e' connesso
     */
    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

}

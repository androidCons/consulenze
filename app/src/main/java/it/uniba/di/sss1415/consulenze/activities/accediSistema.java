package it.uniba.di.sss1415.consulenze.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.rey.material.widget.Button;
import com.rey.material.widget.CheckBox;
import com.rey.material.widget.EditText;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.database.AccediSistemaDB;
import it.uniba.di.sss1415.consulenze.entities.UserData;
import it.uniba.di.sss1415.consulenze.fragments.BachecaFragmentCard;

/**
 * Activity di accesso al sistema. Permette le operazioni di login o di accedere
 * all'activity di registrazione.
 * By Fabrizio Centrone
 */
public class accediSistema extends AppCompatActivity {
    private EditText txtVEmail;
    private EditText txtVPass;
    private CheckBox check;
    private Button loginButton;
    private Button regButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accedi_sistema);

        // controlla accesso automatico
        boolean logout = getIntent().getBooleanExtra(getString(R.string.logout), false);
        if (!logout)  {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            boolean b = sharedPref.getBoolean(getString(R.string.login), false);
            if (b) {
                //vai al menu principale
                Intent intent = new Intent(this, MenuPrincipale.class);
                BachecaFragmentCard.lastSelect = 0;
                MenuPrincipale.lastSelect = 0;
                startActivity(intent);
                finish();
            }
        }
        txtVEmail = (EditText) findViewById(R.id.textfield_email_login);
        txtVPass = (EditText) findViewById(R.id.textfield_password);
        check= (CheckBox) findViewById(R.id.checkBoxLogin);
        controllo_accesso_automatico();

        //LISTENER PER I BOTTONI LOGIN E REGISTRATI
        regButton = (Button) findViewById(R.id.btnRegistrazione);
        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(accediSistema.this, Registrazione.class);
                startActivityForResult(intent, 88);

            }
        });
        loginButton = (Button) findViewById(R.id.btnLogin);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 88){
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_accedi_sistema, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**funzione che genera la stringa di parametri da inviare al server
     *
     * @param tipoElemento richiesta da inviare al server
     * @param accesso puo' essere write, change, delete
     * @param jsonDaInviare il json da inviare
     * @return la stringa di parametri da inviare al server
     */
    public String generaParametri(String tipoElemento, String accesso, String jsonDaInviare){
        //parametri = 'accesso:' + accesso + ', elemento:' + this.tipoElemento + ', jsonDaScrivere:' + jsonDaInviare;
        String stringaP;
        stringaP = "accesso:" + accesso + ", elemento:" + tipoElemento + ", jsonDaScrivere:" + jsonDaInviare;
        return stringaP;
    }

    /**
     * Metodo che si occupa delle operazioni di accesso al sistema, e in caso di esito positivo
     * permette l'accesso al menu principale
     */
    private void login(){
      txtVEmail.clearError();
      txtVPass.clearError();
      String email = txtVEmail.getText().toString();
      String pass = txtVPass.getText().toString();
      Context context = getApplicationContext();
      String error;
      Toast toastMessage;
      // controllo campi
      //la mail deve contenere una chiocciola e un punto
      boolean checkmail = false;
      char c;
      String temp;
      for (int i=0; i<email.length();i++){
          c = email.charAt(i);
          if (c=='@'){
              temp=email.substring(i);
              for (int j=0; j<temp.length();j++){
                  c=temp.charAt(j);
                  if (c=='.') checkmail=true;
              }
          }
      }
      if (checkmail){
          //la password non deve essere vuota
          if (!(pass.compareTo("")==0)){
              //procedi col login
              if(isOnline()) {
                  // async task
                  new AsyncLogin(this).execute();
              } else {
                  // errore connessione internet
                  error = getApplicationContext().getResources().getString(R.string.no_conn);
                  toastMessage = Toast.makeText(context, error, Toast.LENGTH_LONG);
                  toastMessage.show();
              }

          } else {
              //errore password vuota
              txtVPass.setError(getString(R.string.insertPassword));
              error = getApplicationContext().getResources().getString(R.string.insertPassword);
              toastMessage = Toast.makeText(context, error, Toast.LENGTH_LONG);
              toastMessage.show();
          }
      }  else {
          //errore email non valida
          txtVEmail.setError(getString(R.string.errorEmail));
          error = getApplicationContext().getResources().getString(R.string.errorEmail);
          toastMessage = Toast.makeText(context, error, Toast.LENGTH_LONG);
          toastMessage.show();
      }

    }

    private void salva_dati_utente(UserData userdata){
        try {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.clear();
            editor.putString(getString(R.string.email), userdata.getMail());
            editor.putString(getString(R.string.password), userdata.getPass());
            editor.putString(getString(R.string.score), userdata.getScore());
            editor.putString(getString(R.string.txtProv), userdata.getProvincia());
            editor.putString(getString(R.string.numero), userdata.getNumero());
            editor.putString(getString(R.string.anno), userdata.getAnno());
            editor.putString(getString(R.string.nome), userdata.getNome());
            editor.putString(getString(R.string.cognome), userdata.getCognome());
            editor.putString(getString(R.string.spec_prim), userdata.getSpecializzazione());
            // controlla checkbox accesso automatico
            check= (CheckBox) findViewById(R.id.checkBoxLogin);
            editor.putBoolean(getString(R.string.login), check.isChecked()) ;
            editor.apply();
        } catch (Exception e){}
    }

    private void controllo_accesso_automatico(){
        try {
            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            boolean b = sharedPref.getBoolean(getString(R.string.login), false);
            if (b) {
                //inserisci dati dell'utente loggato
                txtVEmail.setText(sharedPref.getString(getString(R.string.email),""));
                txtVPass.setText(sharedPref.getString(getString(R.string.password),""));
                check.setChecked(sharedPref.getBoolean(getString(R.string.login), false));
            }
        } catch (Exception e){}
    }

    /**
     * Controlla se la connessione ad internet e' attiva
     * @return true se e' connesso, false o null altrimenti
     */
    public boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }


    /**
     * Classe che effettua le operazioni di login con un task asincrono
     */
    class AsyncLogin extends AsyncTask<String, Void, Void> {

        ProgressDialog progressDialog;
        Context mContext;
        String email = txtVEmail.getText().toString();
        String pass = txtVPass.getText().toString();

        public AsyncLogin(Context mContext){
            this.mContext = mContext;
        }

        @Override
        protected Void doInBackground(String... params) {
            //connessione al server
            AccediSistemaDB obj = new AccediSistemaDB(getString(R.string.serverQuery),email, pass);
            obj.inviaRichiesta();
            while(obj.parsingComplete);
            if (obj.getRisposta().equals("Utente non presente")){
                //errore login errato
                String error = getApplicationContext().getResources().getString(R.string.errorLogin);
                Toast toastMessage = Toast.makeText(mContext, error, Toast.LENGTH_LONG);
                toastMessage.show();
                txtVEmail.setError("");
                txtVPass.setError("");
            } else {
                //salva dati sulle shared preferences
                UserData userdata = new UserData(obj.getRisposta());
                salva_dati_utente(userdata);
                //VAI AL MENU PRINCIPALE
                Intent intent = new Intent(mContext, MenuPrincipale.class);
                MenuPrincipale.lastSelect = 0;
                BachecaFragmentCard.lastSelect = 0;

                startActivity(intent);
                finish();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            //creazione progress dialog
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage(mContext.getString(R.string.caricamento));
            progressDialog.show();
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            //dismiss della progress dialog
            progressDialog.dismiss();
        }
    }
}

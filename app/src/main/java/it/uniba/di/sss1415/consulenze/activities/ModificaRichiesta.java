package it.uniba.di.sss1415.consulenze.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.rey.material.widget.Button;

import java.util.ArrayList;
import java.util.Calendar;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.adapters.DispTrovModAdapter;
import it.uniba.di.sss1415.consulenze.database.BrancheMedicheDB;
import it.uniba.di.sss1415.consulenze.database.DisponibilitaDB;
import it.uniba.di.sss1415.consulenze.database.InterventiMediciDB;
import it.uniba.di.sss1415.consulenze.database.RichInvDB;
import it.uniba.di.sss1415.consulenze.entities.Disponibilita;

/**
 * Classe per modificare una richiesta inserita
 */
public class ModificaRichiesta extends AppCompatActivity {

    String brancaMedica;
    String interventoMedico;
    DispTrovModAdapter adapter;
    RecyclerView rv;
    Boolean confermaElimina = false;



    /*------ Costanti per Dialog Case ------*/
    static final int SET_DATE = 0;
    static final int SET_TIME = 1;

    Calendar c = Calendar.getInstance();
    private int day = c.get(Calendar.DAY_OF_MONTH);
    private int month = c.get(Calendar.MONTH);
    private int year = c.get(Calendar.YEAR);

    private int hour;
    private int minuteTime;

    public DatePickerDialog.OnDateSetListener setDatePickerListener =
            new DatePickerDialog.OnDateSetListener() {

                // when dialog box is closed, below method will be called.
                public void onDateSet(DatePicker view, int selectedYear,
                                      int selectedMonth, int selectedDay) {
                    year = selectedYear;
                    month = selectedMonth + 1;
                    day = selectedDay;

                    TextView textView = (TextView) findViewById(R.id.textViewData);

                    textView.setText("Data: " + day + "/" + (month) + "/" + year);
                }
            };

    public TimePickerDialog.OnTimeSetListener setTimePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    minuteTime = minute;
                    hour = hourOfDay;
                    TextView textView = (TextView) findViewById(R.id.textViewStartTime);
                    textView.setText("Orario inizio: " + hour + ":" + minuteTime);
                }
            };

    ArrayList<Disponibilita> dispList;
    ArrayList<Disponibilita> dispListFiltrata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifica_richiesta);

        final String id = getIntent().getExtras().getString("id");

        interventoMedico = getIntent().getExtras().getString("interventoMedico");
        TextView description = (TextView) findViewById(R.id.textViewDescriptionRequest);
            description.setText("Intervento di: " + interventoMedico);

        ImageButton imgBtnDate = (ImageButton) findViewById(R.id.imgBtnDateRequest);
        imgBtnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_DATE).show();
            }
        });

        ImageButton imgBtnTime = (ImageButton) findViewById(R.id.imgBtnTimeRequest);
        imgBtnTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_TIME).show();
            }
        });

        brancaMedica = queryBrancheMediche();
        DisponibilitaDB dispTrovate = new DisponibilitaDB(getString(R.string.serverQuery));
        dispTrovate.inviaRichiestaLettura();
        while(dispTrovate.parsingComplete);
        dispList = dispTrovate.getdispList();
        pulisciLista();
        dispListFiltrata = dispList;
        filtraSpecializzazioneDispList();
        if(dispListFiltrata.size() != 0){
            rv = (RecyclerView) findViewById(R.id.cardListDispTrovate);
            rv.setVisibility(View.VISIBLE);
            LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
            rv.setLayoutManager(llm);
            if(interventoMedico.length() != 0)
                adapter = new DispTrovModAdapter(dispListFiltrata, interventoMedico);
            else
                adapter = new DispTrovModAdapter(dispListFiltrata, brancaMedica);
            rv.setAdapter(adapter);
        }else{
            TextView nessunaDisponibilita = (TextView) findViewById(R.id.textViewNessunaDisp);
            nessunaDisponibilita.setVisibility(View.VISIBLE);

        }


        final Button btnElimina = (Button) findViewById(R.id.btnElimina);
        btnElimina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplication());
                String email = sharedPreferences.getString("Email", "ciao@afa.it") ;
                if (!confermaElimina){
                    confermaElimina=false;
                    btnElimina.setBackgroundColor(getResources().getColor(R.color.button_material_light));

                    btnElimina.setBackgroundColor(getResources().getColor(R.color.confirm_button));
                    btnElimina.setTextColor(getResources().getColor(R.color.carbon_black));
                    confermaElimina=true;
                    Toast.makeText(getApplicationContext(), getString(R.string.conferma_cancella), Toast.LENGTH_SHORT).show();
                }
                else{
                    confermaElimina=false;
                    btnElimina.setBackgroundColor(getResources().getColor(R.color.button_material_light));
                    RichInvDB richInvDB1 = new RichInvDB(getString(R.string.serverQuery),
                            id, "", "",
                            "", "", email);
                    richInvDB1.inviaRichiestaCancella();
                    Toast.makeText(getApplicationContext(), getString(R.string.richiesta_cancellata), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.rlModRich);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confermaElimina = false;
                btnElimina.setBackgroundColor(getResources().getColor(R.color.accent));
                btnElimina.setTextColor(getResources().getColor(R.color.white));
            }
        });


    }

    private String queryBrancheMediche(){

        BrancheMedicheDB obj = new BrancheMedicheDB(
                getString(R.string.serverQuery)
        );
        obj.inviaRichiesta();

        while(obj.parsingComplete);
        //stampa a video della stringa
        String lista = obj.getName();

        char c;
        String temp="";
        ArrayList<String> list = new ArrayList<String>();
        for (int i=0; i<lista.length();i++){
            c=lista.charAt(i);
            if (c==','|| i==lista.length()){
                list.add(temp);
                temp="";
            } else temp = temp + c;

        }
        Boolean trovato = false;
        int i=-1;
        ArrayList<String> interventi;
        try{
            while(!trovato){
                i=i+1;
                interventi = queryInterventiMedici(list.get(i));
                if (interventi.size() != 0){
                    for(int j = 0; j < interventi.size(); j++){
                        if (interventi.get(j).compareTo(interventoMedico) == 0)
                            trovato = true;
                    }
                }
            }
            return list.get(i);
        }catch(IndexOutOfBoundsException e){
            Toast.makeText(getApplicationContext(), getString(R.string.errore), Toast.LENGTH_SHORT).show();
            finish();
        }
        return  list.get(i-1);
    }

    private ArrayList<String> queryInterventiMedici(String specializzazione){

        InterventiMediciDB obj = new InterventiMediciDB(
                getString(R.string.serverQuery), specializzazione
        );
        obj.inviaRichiesta();

        while(obj.parsingComplete);
        //stampa a video della stringa
        String lista = obj.getName();

        char c;
        String temp="";
        ArrayList<String> list = new ArrayList<String>();
        for (int i=0; i<lista.length();i++){
            c=lista.charAt(i);
            if (c==','|| i==lista.length()){
                list.add(temp);
                temp="";
            } else temp = temp + c;

        }
        return list;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nuova_richiesta2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private Dialog createdDialog(int id) {
        switch (id) {
            case (SET_DATE): {
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, setDatePickerListener, year, month-1,
                        day);
                return datePickerDialog;
            }
            case (SET_TIME): {
                TimePickerDialog timePickerDialog = new TimePickerDialog(this, setTimePickerListener, 0, 0, true);
                return timePickerDialog;
            }
        }
        return null;
    }


    private void pulisciLista() {
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH) + 1;
        int year = c.get(Calendar.YEAR);

        /*elimino disponibilita passate*/
        for (int i = 0; i < dispList.size(); i++){
            if (Integer.parseInt(dispList.get(i).getData().substring(0,4)) < year){
                dispList.remove(i);
                i = i-1;
            }

            else{
                if (Integer.parseInt(dispList.get(i).getData().substring(0,4)) == year){
                    if (Integer.parseInt(dispList.get(i).getData().substring(5,7)) < month){
                        dispList.remove(i);
                        i = i-1;
                    }
                    else{
                        if (Integer.parseInt(dispList.get(i).getData().substring(5,7)) == month) {
                            if (Integer.parseInt(dispList.get(i).getData().substring(8,10)) < day){
                                dispList.remove(i);
                                i = i-1;
                            }
                        }
                    }
                }
            }
        }

        /*Ordino per data crescente*/
        for (int i=0; i < dispList.size(); i++){

            for (int j = 1; j < dispList.size()-i; j++){
                if(Integer.parseInt(dispList.get(j-1).getData().substring(0,4)) >
                        Integer.parseInt(dispList.get(j).getData().substring(0,4))){
                    Disponibilita temp = dispList.get(j-1);
                    dispList.set(j-1, dispList.get(j));
                    dispList.set(j, temp);
                }
                else{
                    if(Integer.parseInt(dispList.get(j-1).getData().substring(0,4)) ==
                            Integer.parseInt(dispList.get(j).getData().substring(0,4))){
                        if(Integer.parseInt(dispList.get(j-1).getData().substring(5,7)) >
                                Integer.parseInt(dispList.get(j).getData().substring(5,7))){
                            Disponibilita temp = dispList.get(j-1);
                            dispList.set(j-1, dispList.get(j));
                            dispList.set(j, temp);
                        }
                        else{
                            if (Integer.parseInt(dispList.get(j-1).getData().substring(5,7)) ==
                                    Integer.parseInt(dispList.get(j).getData().substring(5,7))){
                                if(Integer.parseInt(dispList.get(j-1).getData().substring(8,10)) >
                                        Integer.parseInt(dispList.get(j).getData().substring(8,10))){
                                    Disponibilita temp = dispList.get(j-1);
                                    dispList.set(j-1, dispList.get(j));
                                    dispList.set(j, temp);
                                }
                            }
                        }
                    }
                }

            }
        }
    }

    private void filtraSpecializzazioneDispList() {
        for(int i = 0; i < dispListFiltrata.size(); i++){
            if (dispListFiltrata.get(i).getIntervento().compareTo(brancaMedica) != 0){
                dispListFiltrata.remove(i);
                i=i-1;
            }

        }
    }
}

package it.uniba.di.sss1415.consulenze.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonFloat;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.database.TutorDB;

/**
 * Activity per la valutazione di un tutor.
 * By Emanuele Pio Barracchia
 */
public class ValutaTutor extends AppCompatActivity {


    private ButtonFloat confVoto;
    private LinearLayout moltoSodd;
    private LinearLayout sodd;
    private LinearLayout nonMoltoSodd;
    private TextView txtVTutor;
    private TextView txtVMoltoSodd;
    private TextView txtVSodd;
    private TextView txtVNonMoltoSodd;
    private ImageView imgVMoltoSodd;
    private ImageView imgVSodd;
    private ImageView imgVNonMoltoSodd;
    private static STATUS stato = STATUS.NOT_DEFINED;

    private View.OnClickListener moltoSoddListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (stato == STATUS.NOT_DEFINED) {
                imgVMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon));
                txtVMoltoSodd.setTextColor(getResources().getColor(R.color.green_dark));
                confVoto.setEnabled(true);
                stato = STATUS.HIGH;
            } else if (stato == STATUS.HIGH) {
                imgVMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon));
                txtVMoltoSodd.setTextColor(getResources().getColor(R.color.carbon_black));
                confVoto.setEnabled(false);
                stato = STATUS.NOT_DEFINED;
            } else if (stato == STATUS.NORMAL) {
                imgVMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon));
                txtVMoltoSodd.setTextColor(getResources().getColor(R.color.green_dark));
                confVoto.setEnabled(true);
                imgVSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon_happy));
                txtVSodd.setTextColor(getResources().getColor(R.color.carbon_black));
                stato = STATUS.HIGH;
            } else if (stato == STATUS.LOW) {
                imgVMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon));
                txtVMoltoSodd.setTextColor(getResources().getColor(R.color.green_dark));
                confVoto.setEnabled(true);
                imgVNonMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon_sad));
                txtVNonMoltoSodd.setTextColor(getResources().getColor(R.color.carbon_black));
                stato = STATUS.HIGH;
            }
        }
    };

    private View.OnClickListener soddListenser = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (stato == STATUS.NOT_DEFINED) {
                imgVSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon_happy));
                txtVSodd.setTextColor(getResources().getColor(R.color.yellow));
                confVoto.setEnabled(true);
                stato = STATUS.NORMAL;
            } else if (stato == STATUS.HIGH) {
                imgVSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon_happy));
                txtVSodd.setTextColor(getResources().getColor(R.color.yellow));
                confVoto.setEnabled(true);
                imgVMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon));
                txtVMoltoSodd.setTextColor(getResources().getColor(R.color.carbon_black));
                stato = STATUS.NORMAL;
            } else if (stato == STATUS.NORMAL) {
                imgVSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon_happy));
                txtVSodd.setTextColor(getResources().getColor(R.color.carbon_black));
                confVoto.setEnabled(false);
                stato = STATUS.NOT_DEFINED;
            } else if (stato == STATUS.LOW) {
                imgVSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon_happy));
                txtVSodd.setTextColor(getResources().getColor(R.color.yellow));
                confVoto.setEnabled(true);
                imgVNonMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon_sad));
                txtVNonMoltoSodd.setTextColor(getResources().getColor(R.color.carbon_black));
                stato = STATUS.NORMAL;
            }
        }
    };

    private View.OnClickListener nonMoltoSoddListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (stato == STATUS.NOT_DEFINED) {
                imgVNonMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon_sad));
                txtVNonMoltoSodd.setTextColor(getResources().getColor(R.color.red));
                confVoto.setEnabled(true);
                stato = STATUS.LOW;
            } else if (stato == STATUS.HIGH) {
                imgVNonMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon_sad));
                txtVNonMoltoSodd.setTextColor(getResources().getColor(R.color.red));
                confVoto.setEnabled(true);
                imgVMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon));
                txtVMoltoSodd.setTextColor(getResources().getColor(R.color.carbon_black));
                stato = STATUS.LOW;
            } else if (stato == STATUS.NORMAL) {
                imgVNonMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon_sad));
                txtVNonMoltoSodd.setTextColor(getResources().getColor(R.color.red));
                confVoto.setEnabled(true);
                imgVSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon_happy));
                txtVSodd.setTextColor(getResources().getColor(R.color.carbon_black));
                stato = STATUS.LOW;
            } else if (stato == STATUS.LOW) {
                imgVNonMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon_sad));
                txtVNonMoltoSodd.setTextColor(getResources().getColor(R.color.carbon_black));
                confVoto.setEnabled(false);
                stato = STATUS.NOT_DEFINED;
            }


        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valuta_consulenza);


        txtVTutor = (TextView) findViewById(R.id.txtVTutorVal);
        Intent intent = getIntent();

        txtVTutor.setText(intent.getStringExtra("NomeT") + " " + intent.getStringExtra("CognT"));

        confVoto = (ButtonFloat) findViewById(R.id.btnConfVoto);
        confVoto.setEnabled(false);
        confVoto.setDrawableIcon(getResources().getDrawable(R.drawable.check));
        moltoSodd = (LinearLayout) findViewById(R.id.btnMoltoSodd);
        sodd = (LinearLayout) findViewById(R.id.btnSodd);
        nonMoltoSodd = (LinearLayout) findViewById(R.id.btnNonMoltoSodd);
        imgVMoltoSodd = (ImageView) findViewById(R.id.imgVMoltoSodd);
        txtVMoltoSodd = (TextView) findViewById(R.id.txtVMoltoSodd);
        imgVSodd = (ImageView) findViewById(R.id.imgVSodd);
        txtVSodd = (TextView) findViewById(R.id.txtVSodd);
        imgVNonMoltoSodd = (ImageView) findViewById(R.id.imgVNonMolto);
        txtVNonMoltoSodd = (TextView) findViewById(R.id.txtVNonMoltoSodd);
        imgVMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon));
        imgVSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon_happy));
        imgVNonMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.ic_emoticon_sad));


        moltoSodd.setOnClickListener(moltoSoddListener);
        sodd.setOnClickListener(soddListenser);
        nonMoltoSodd.setOnClickListener(nonMoltoSoddListener);

        if (stato == STATUS.HIGH) {
            imgVMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon));
            txtVMoltoSodd.setTextColor(getResources().getColor(R.color.green_dark));
            confVoto.setEnabled(true);
        } else if (stato == STATUS.NORMAL) {
            imgVSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon_happy));
            txtVSodd.setTextColor(getResources().getColor(R.color.yellow));
            confVoto.setEnabled(true);
        } else if (stato == STATUS.LOW) {
            imgVNonMoltoSodd.setImageDrawable(getResources().getDrawable(R.drawable.emoticon_sad));
            txtVNonMoltoSodd.setTextColor(getResources().getColor(R.color.red));
            confVoto.setEnabled(true);
        }

        confVoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //istruzioni per mandare il voto al server
                int score = Integer.parseInt(getIntent().getStringExtra("Score"));
                if (stato == STATUS.HIGH) {
                    score += 2;
                } else if (stato == STATUS.NORMAL) {
                    score += 1;
                } else {
                    score -= 1;
                }
                String nome = getIntent().getStringExtra("NomeT");
                String cogn = getIntent().getStringExtra("CognT");

                TutorDB tutorDB = new TutorDB(
                        getString(R.string.serverQuery),
                        nome,
                        cogn,
                        String.valueOf(score));
                tutorDB.modificaScore();
                while (tutorDB.parsingComplete) ;
                Toast toast = Toast.makeText(getApplicationContext(), "Voto inviato", Toast.LENGTH_SHORT);
                toast.show();
                stato = STATUS.NOT_DEFINED;

                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_valuta_consulenza, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }


    private enum STATUS {
        NOT_DEFINED,
        HIGH,
        NORMAL,
        LOW
    }
}

package it.uniba.di.sss1415.consulenze.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import it.uniba.di.sss1415.consulenze.activities.ModificaDisponibilita;
import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.entities.Disponibilita;

/**
 * Adapter per le disponibilita' inserite
 */
public class DisponibilitaInseriteAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Disponibilita> dispList;



    public DisponibilitaInseriteAdapter(Context c, ArrayList<Disponibilita> dispList) {
        mContext = c;
        this.dispList = dispList;
    }


    public int getCount() {
        return dispList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {


        View view = LayoutInflater.from(mContext).inflate(R.layout.lista_disponibilita_inserite, null);
        TextView txtData = (TextView) view.findViewById(R.id.textViewData);
        TextView txtOrario = (TextView) view.findViewById(R.id.textViewOrario);
        TextView txtDescription = (TextView) view.findViewById(R.id.textViewDescription);
        TextView txtRepetition = (TextView) view.findViewById(R.id.textViewRepetition);
        TextView txtEndRepetition = (TextView) view.findViewById(R.id.textViewEndDate);
        ImageButton imgBtnModifica = (ImageButton) view.findViewById(R.id.imgBtnModify);
        txtData.setText(dispList.get(position).getData());
        txtOrario.setText("Dalle " + dispList.get(position).getOraInizio().substring(0,5) + "\nalle " + dispList.get(position).getOraFine().substring(0,5));
        txtDescription.setText("Expertise offerta:\n" + dispList.get(position).getIntervento());
        txtRepetition.setText(dispList.get(position).getRipetizione());
        if(txtRepetition.getText().length() != 0) txtEndRepetition.setText(dispList.get(position).getFineRipetizione());
        else txtEndRepetition.setText("");
        imgBtnModifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ModificaDisponibilita.class);
                intent.putExtra("id", dispList.get(position).getId());
                intent.putExtra("data", dispList.get(position).getData());
                intent.putExtra("ora_inizio", dispList.get(position).getOraInizio());
                intent.putExtra("ora_fine", dispList.get(position).getOraFine());
                intent.putExtra("intervento", dispList.get(position).getIntervento());
                intent.putExtra("ripetizione", dispList.get(position).getRipetizione());
                intent.putExtra("fineRipetizione", dispList.get(position).getFineRipetizione());
                ((Activity) mContext).startActivityForResult(intent, 0);
            }
        });
        return view;
    }

}
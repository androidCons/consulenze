package it.uniba.di.sss1415.consulenze.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Calendar;

import it.uniba.di.sss1415.consulenze.R;
import it.uniba.di.sss1415.consulenze.database.DisponibilitaDB;
import it.uniba.di.sss1415.consulenze.other.Utility;

import static java.lang.Integer.parseInt;
import static java.lang.Integer.toString;


public class NuovaDisponibilita extends AppCompatActivity {



    /* Variabili per inserimento Disponibilita */
    private String specializzazione;
    private String email;

    private int startMinuteTime = 0;
    private int startHourTime = 0;
    private int endDay;
    private int endMonth;
    private int endYear;
    private int endMinuteTime;
    private int endHourTime;
    private boolean ripetizioneAttiva = false;
    private String ripetizione ="";
    private SharedPreferences sharedPreferences;
    private boolean confirm;
    private TextView txtData;
    private TextView txtEndDate;
    private TextView txtStartTime;
    private TextView txtEndTime;
    private TextView txtDescr;
    private ToggleButton toggleButton;
    private RadioGroup radioGroup;
    private Toast toast;
    private Boolean controlloStartDate = false;
    private Boolean controlloEndDate = true;
    private Boolean controlloEndTime = false;
    private Boolean controlloStartTime = false;

    /*------ Costanti per Dialog Case ------*/
    static final int SET_START_DATE = 0;
    static final int SET_END_DATE = 1;
    static final int SET_START_TIME = 2;
    static final int SET_END_TIME = 3;


    /*-------Inizio dichiarazioni variabili per Data Picker -------*/
    Calendar c = Calendar.getInstance();
    private int startDay = c.get(Calendar.DAY_OF_MONTH);
    private int startMonth = c.get(Calendar.MONTH)+1;
    private int startYear = c.get(Calendar.YEAR);
    private int todayDay = c.get(Calendar.DAY_OF_MONTH);
    private int todayMonth = c.get(Calendar.MONTH);
    private int todayYear = c.get(Calendar.YEAR);
    private int nameDay;
    private String[] giorniSettimana= {"Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato", "Domenica"};

    public DatePickerDialog.OnDateSetListener startDatePickerListener =
            new DatePickerDialog.OnDateSetListener() {

                // when dialog box is closed, below method will be called.
                public void onDateSet(DatePicker view, int selectedYear,
                                      int selectedMonth, int selectedDay) {

                    controlloStartDate = true;
                    if(selectedYear < todayYear){
                        controlloStartDate = false;
                    }else{
                        if(selectedYear == todayYear){
                            if(selectedMonth < todayMonth){
                                controlloStartDate = false;
                            }else{
                                if (selectedMonth == todayMonth){
                                    if (selectedDay < todayDay){
                                        controlloStartDate = false;
                                    }
                                }
                            }
                        }
                    }

                    if(controlloStartDate){
                        startYear = selectedYear;
                        startMonth = selectedMonth + 1;
                        startDay = selectedDay;
                        txtData.setText("Data: " + Utility.formattaDataOra(startDay) + "/" + Utility.formattaDataOra(startMonth) + "/" + startYear);
                    }else{
                        toast = Toast.makeText(getApplicationContext(), "La data deve essere maggiore" +
                                " o uguale a quella di oggi", Toast.LENGTH_SHORT);
                        toast.show();
                        txtData.setText(R.string.inserisci_data);
                    }

                }
            };

    public DatePickerDialog.OnDateSetListener endDatePickerListener =
            new DatePickerDialog.OnDateSetListener() {

                // when dialog box is closed, below method will be called.
                public void onDateSet(DatePicker view, int selectedYear,
                                      int selectedMonth, int selectedDay) {

                    controlloEndDate = true;
                    if(selectedYear < startYear){
                        controlloEndDate = false;
                    }else{
                        if(selectedYear == startYear){
                            if(selectedMonth + 1  < startMonth){
                                controlloEndDate = false;
                            }else{
                                if (selectedMonth + 1 == startMonth){
                                    if (selectedDay < startDay){
                                        controlloEndDate = false;
                                    }
                                }
                            }
                        }
                    }

                    if(controlloEndDate){
                        endYear = selectedYear;
                        endMonth = selectedMonth + 1;
                        endDay = selectedDay;
                        txtEndDate.setText("Data: " + Utility.formattaDataOra(endDay) + "/" + Utility.formattaDataOra(endMonth) + "/" + endYear);
                    }else{
                        toast = Toast.makeText(getApplicationContext(), "La data di fine deve essere maggiore" +
                                " o uguale a quella di inizio", Toast.LENGTH_SHORT);
                        toast.show();
                        txtEndDate.setText("Iposta data scadenza");
                    }
                }
            };


    /*-------Inizio dichiarazioni variabili per Data Picker -------*/

    public TimePickerDialog.OnTimeSetListener startTimePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                    controlloStartTime = true;

                    startMinuteTime = minute;
                    startHourTime = hourOfDay;

                    txtStartTime.setText("Orario inizio: " + Utility.formattaDataOra(startHourTime) + ":" + Utility.formattaDataOra(startMinuteTime));


                }
            };

    public TimePickerDialog.OnTimeSetListener endTimePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                    controlloEndTime = true;
                    if(hourOfDay < startHourTime){
                        controlloEndTime = false;
                    }else{
                        if (hourOfDay == startHourTime){
                            if (minute <= startMinuteTime){
                                controlloEndTime = false;
                            }
                        }
                    }
                    if(controlloEndTime){
                        endMinuteTime = minute;
                        endHourTime = hourOfDay;

                        txtEndTime.setText("Orario Fine: " + Utility.formattaDataOra(endHourTime) +
                                ":" + Utility.formattaDataOra(endMinuteTime));
                    }else{
                        toast = Toast.makeText(getApplicationContext(), "L'orario di fine deve essere " +
                                "maggiore a quella di inizio", Toast.LENGTH_SHORT);
                        toast.show();
                        txtEndTime.setText("Imposta ora fine");
                    }



                }
            };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuova_disponibilita);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        specializzazione = sharedPreferences.getString("Specializzazione primaria", "Allergologia");
        txtDescr = (TextView) findViewById(R.id.textViewDescription);
        txtDescr.setText("Stai inserendo una disponibilita' di tutoraggio per: " + specializzazione);
        txtEndTime = (TextView) findViewById(R.id.textViewEndTime);
        txtEndDate = (TextView) findViewById(R.id.textViewEndDate);
        txtData = (TextView) findViewById(R.id.textViewData);
        txtStartTime = (TextView) findViewById(R.id.textViewStartTime);

        ImageButton imgBtnStartDate = (ImageButton) findViewById(R.id.imgBtnCalendar);
        imgBtnStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_START_DATE).show();
            }
        });

        txtData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_START_DATE).show();
            }
        });

        ImageButton imgBtnEndDate = (ImageButton) findViewById(R.id.imgBtnEndDate);
        imgBtnEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_END_DATE).show();
            }
        });

        txtEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_END_DATE).show();
            }
        });

        ImageButton imgBtnStartTime = (ImageButton) findViewById(R.id.imgBtnStartTime);
        imgBtnStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_START_TIME).show();
            }
        });

        txtStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_START_TIME).show();
            }
        });

        ImageButton imgBtnEndTime = (ImageButton) findViewById(R.id.imgBtnEndTime);
        imgBtnEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_END_TIME).show();
            }
        });

        txtEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createdDialog(SET_END_TIME).show();
            }
        });

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        toggleButton = (ToggleButton) findViewById(R.id.ToggleButtonRipetizione);
        if(savedInstanceState != null){
            txtData.setText(savedInstanceState.getString("Data"));
            txtDescr.setText(savedInstanceState.getString("Descr"));
            txtStartTime.setText(savedInstanceState.getString("StartTime"));
            txtEndTime.setText(savedInstanceState.getString("EndTime"));
            if (savedInstanceState.getBoolean("RipetEnabled") == true){
                toggleButton.setChecked(true);
                radioGroup.setVisibility(View.VISIBLE);
                radioGroup.check(savedInstanceState.getInt("Ripet"));
                txtEndDate.setText(savedInstanceState.getString("EndDate"));
                txtEndDate.setVisibility(View.VISIBLE);
                imgBtnEndDate.setVisibility(View.VISIBLE);
                controlloEndTime = false;

            } else {
                toggleButton.setChecked(false);
                radioGroup.setVisibility(View.INVISIBLE);
                controlloEndDate = true;
            }
        }
        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean on = ((ToggleButton) v).isChecked();


                if (on) {
                    radioGroup.setVisibility(View.VISIBLE);
                    ImageButton imgbtnEndDate = (ImageButton) findViewById(R.id.imgBtnEndDate);
                    imgbtnEndDate.setVisibility(View.VISIBLE);
                    txtEndDate = (TextView) findViewById(R.id.textViewEndDate);
                    txtEndDate.setVisibility(View.VISIBLE);
                    ripetizioneAttiva = true;
                    controlloEndDate = false;
                } else {
                    radioGroup.setVisibility(View.INVISIBLE);
                    ImageButton imgbtnEndDate = (ImageButton) findViewById(R.id.imgBtnEndDate);
                    imgbtnEndDate.setVisibility(View.INVISIBLE);
                    txtEndDate = (TextView) findViewById(R.id.textViewEndDate);
                    txtEndDate.setVisibility(View.INVISIBLE);
                    ripetizioneAttiva = false;
                    ripetizione = "";
                    controlloEndDate = true;
                }

            }
        });

        final Button btnConferma = (Button) findViewById(R.id.btnConferma);
        btnConferma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (controlloEndTime && controlloStartDate && controlloEndDate && controlloStartTime){
                    if (!confirm){
                        btnConferma.setBackgroundColor(getResources().getColor(R.color.confirm_button));
                        btnConferma.setTextColor(getResources().getColor(R.color.carbon_black));
                        confirm=true;
                        btnConferma.setText(R.string.conf);
                    }
                    else {
                        confirm=false;
                        btnConferma.setBackgroundColor(getResources().getColor(R.color.button_material_light));
                        inserimentoNuovaDisponibilita();
                        setResult(RESULT_OK);
                        finish();
                    }
                }else{
                    toast = Toast.makeText(getApplicationContext(), "Si è verificato un errore, ricontrollare" +
                            " i campi", Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        });

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.rlNuovDisp);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnConferma.setTextColor(getResources().getColor(R.color.white));
                btnConferma.setBackgroundColor(getResources().getColor(R.color.accent));
                btnConferma.setText(getString(R.string.inserisci));
                confirm = false;
            }
        });


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("Data", txtData.getText().toString());
        outState.putString("Descr", txtDescr.getText().toString());
        outState.putString("StartTime", txtStartTime.getText().toString());
        outState.putString("EndTime", txtEndTime.getText().toString());
        outState.putBoolean("RipetEnabled", toggleButton.isChecked());
        outState.putInt("Ripet", radioGroup.getCheckedRadioButtonId());
        outState.putString("EndDate", txtEndDate.getText().toString());
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nuova_disponibilita, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }


    /*-------Date Picker--------------*/
    private Dialog createdDialog(int id) {
        switch (id) {
            case (SET_START_DATE): {
                DatePickerDialog startDatePickerDialog = new DatePickerDialog(this, startDatePickerListener, startYear, startMonth-1,
                        startDay);
                return startDatePickerDialog;
            }
            case (SET_END_DATE): {
                DatePickerDialog endDatePickerDialog = new DatePickerDialog(this, endDatePickerListener, startYear, startMonth-1,
                        startDay);
                return endDatePickerDialog;
            }
            case (SET_START_TIME): {
                TimePickerDialog startTimePickerDialog = new TimePickerDialog(this, startTimePickerListener, startHourTime, startMinuteTime, true);
                return startTimePickerDialog;
            }
            case (SET_END_TIME): {
                TimePickerDialog endTimePickerDialog = new TimePickerDialog(this, endTimePickerListener, startHourTime, startMinuteTime, true);
                return endTimePickerDialog;
            }
        }
        return null;
    }

    private void inserimentoNuovaDisponibilita(){

        email = sharedPreferences.getString("Email", "ciao@afa.it");
        String data = startYear + "-" + startMonth + "-" + startDay;
        String ora_inizio = startHourTime + ":" + startMinuteTime;
        String ora_fine = endHourTime + ":" + endMinuteTime;


       /*------NON FUNZIONA ------
        if(ripetizioneAttiva){
            RadioButton radioButton = (RadioButton) findViewById(R.id.radioButtonEveryWeek);
            c.set(startYear, startMonth, startDay);

           // if(radioButton.isChecked()) ripetizione = "Ogni settimana (" +
        } */


        /*----- INSERIRE METODO PER RICAVARE L'ULTIMO ID ------*/
        DisponibilitaDB disponibilitaDB = new DisponibilitaDB(getString(R.string.serverQuery));
        disponibilitaDB.inviaRichiestaLettura();
        while(disponibilitaDB.parsingComplete);

        int id = Integer.parseInt(disponibilitaDB.lastId());
        id=id+1;
        DisponibilitaDB obj = new DisponibilitaDB(getString(R.string.serverQuery),String.valueOf(id), data, ora_inizio, ora_fine, specializzazione);
        obj.inviaRichiestaScrittura();

    }


}

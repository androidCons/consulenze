package it.uniba.di.sss1415.consulenze.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import it.uniba.di.sss1415.consulenze.R;

/**
 * Adapter per la visualizzazione degli elementi del drawer.
 * By Emanuele Pio Barracchia
 */
public class DrawerAdapter extends BaseAdapter {

    private Context context;
    private String[] values;
    private Drawable drawable;
    public DrawerAdapter(Context context, String[] values){
        this.context = context;
        this.values = values;
    }

    public int getCount(){
        return values.length;
    }
    @Override
    public Object getItem(int position) {
        return values[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.drawer_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView) convertView.findViewById(R.id.drawer_item_image);
            viewHolder.text = (TextView) convertView.findViewById(R.id.drawer_item_text);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        switch (position){
            case 0:
                viewHolder.image.setImageDrawable(drawable);
                viewHolder.text.setText(context.getResources().getText(R.string.title_activity_bacheca));
                break;
            default:
        }


        viewHolder.text.setText(values[position]);
        return convertView;
    }

    private class ViewHolder {
        ImageView image;
        TextView text;
    }
}
